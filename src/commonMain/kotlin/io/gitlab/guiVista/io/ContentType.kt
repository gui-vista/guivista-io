package io.gitlab.guiVista.io

import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.io.file.File
import io.gitlab.guiVista.io.icon.IconBase

/** Platform specific content typing. */
public expect object ContentType {
    /**
     * Compares two content types for equality.
     * @param type1 A content type.
     * @param type2 A content type.
     * @return A value of *true* if the two strings are identical or equivalent.
     */
    public fun equalTypes(type1: String, type2: String): Boolean

    /**
     * Determines if [type] is a subset of [superType].
     * @param type A content type.
     * @param superType A content type that is the super type.
     * @return A value of *true* if [type] is a kind of [superType].
     */
    public fun isSubset(type: String, superType: String): Boolean

    /**
     * Determines if type is a subset of mime_type . Convenience wrapper around [isSubset].
     * @param type A content type.
     * @param mimeType A mime type.
     * @return A value of *true* if [type] is a kind of [mimeType].
     */
    public fun isMimeType(type: String, mimeType: String): Boolean

    /**
     * Checks if the [content type][type] is the generic **unknown** type. On UNIX this is the
     * **application/octet-stream** mime type.
     * @param type A content type.
     * @return A value of *true* if [type] is the unknown type.
     */
    public fun isUnknown(type: String): Boolean

    /**
     * Gets the human readable description of the content type.
     * @param type A content type.
     * @return A short description of the [content type][type].
     */
    public fun fetchDescription(type: String): String

    /**
     * Gets the mime type for the content type if one is registered.
     * @param type The content type.
     * @return The registered mime type for the given type, or *""* (an empty String) if unknown.
     */
    public fun fetchMimeType(type: String): String

    /**
     * Gets the icon for a content type.
     * @param type A content type.
     * @return A [IconBase] corresponding to the content type. Remember to call the **close** function when you are
     * finished with the returned object.
     */
    public fun fetchIcon(type: String): IconBase

    /**
     * Gets the symbolic icon for a content type.
     * @param type A content type.
     * @return A [IconBase] corresponding to the content type. Remember to call the **close** function when you are
     * finished with the returned object.
     */
    public fun fetchSymbolicIcon(type: String): IconBase

    /**
     * Gets the generic icon name for a content type. Refer to the shared-mime-info specification for more on the
     * generic icon name.
     * @param type A content type.
     * @return The registered generic icon name for the given type, or *""* (an empty String) if unknown.
     */
    public fun fetchGenericIconName(type: String): String

    /**
     * Checks if a content type can be executable. Note that for instance things like text files can be executables
     * (i.e. scripts and batch files).
     * @param type A content type.
     * @return A value of *true* if the file type corresponds to a type that can be executable.
     */
    public fun canBeExecutable(type: String): Boolean

    /**
     * Tries to find a content type based on the mime type name.
     * @param mimeType The mime type.
     * @return The content type or *""* (an empty String).
     */
    public fun fromMimeType(mimeType: String): String

    /**
     * Gets a list of strings containing all the registered content types known to the system. The list and its data
     * should be freed using the **closeAll** function.
     * @return A list of the registered content types.
     */
    public fun fetchRegistered(): DoublyLinkedList

    /**
     * Tries to guess the type of the tree with [root] by looking at the files it contains. The result is an array of
     * content types, with the best guess coming first. The types returned all have the form x-content/foo, e.g.
     * x-content/audio-cdda (for audio CDs), or x-content/image-dcf (for a camera memory card). See the
     * shared-mime-info specification for more on x-content types.
     *
     * This function is useful in the implementation of `g_mount_guess_content_type()`.
     * @return An array of zero or more content types.
     */
    public fun guessForTree(root: File): Array<String>

    /**
     * Guesses the content type based on example data. If the function is uncertain then resultUncertain will be set to
     * *true*. Either [fileName], or data may be empty in which case the guess will be based solely on the other
     * argument.
     * @param fileName A [String] or *""* (an empty String).
     * @param data A stream of data or an empty array.
     * @return A Pair containing the following:
     * 1. resultUncertain - If *true* then the result isn't certain.
     * 2. contentType - The content type.
     */
    public fun guess(fileName: String, data: UByteArray): Pair<Boolean, String>
}
