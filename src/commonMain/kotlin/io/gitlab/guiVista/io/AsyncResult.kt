package io.gitlab.guiVista.io

import io.gitlab.guiVista.core.ObjectBase

/** Asynchronous function results. */
public expect interface AsyncResult {
    /** The source object from a [AsyncResult]. */
    public open val srcObj: ObjectBase?
}
