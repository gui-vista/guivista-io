package io.gitlab.guiVista.io

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.ObjectBase

/** Thread-safe operation cancellation stack. */
public expect class Cancellable : ObjectBase {
    /** Checks if a cancellable job has been cancelled. */
    public val isCancelled: Boolean

    /**
     * Gets the file descriptor for a cancellable job. This can be used to implement cancellable operations on Unix
     * systems. The returned fd will turn readable when cancellable is cancelled. You are not supposed to read from the
     * fd yourself, just check for readable status. Reading to unset the readable status is done with [reset].
     *
     * After a successful return from this function, you should use [releaseFileDescriptor] to free up resources
     * allocated for the returned file descriptor. See also `g_cancellable_make_pollfd()`.
     */
    public val fileDescriptor: Int

    public companion object {
        /** The top cancellable from the stack, or *null* if the stack is empty. */
        public val current: Cancellable?

        /**
         * Creates a new [Cancellable] object. Applications that want to start one or more operations that should be
         * cancellable should create a [Cancellable] and pass it to the operations. One [Cancellable] can be used in
         * multiple consecutive operations, or in multiple concurrent operations.
         * @return A new instance of [Cancellable].
         */
        public fun create(): Cancellable
    }

    /**
     * If the cancellable is cancelled, sets the error to notify that the operation was cancelled.
     * @param error The error to use to append state to.
     * @return A value of *true* if [Cancellable] was cancelled.
     */
    public fun changeErrorIfCancelled(error: Error): Boolean

    /**
     * Releases a resources previously allocated by [fileDescriptor], or `g_cancellable_make_pollfd()`. For
     * compatibility reasons with older releases, calling this function is not strictly required, the resources will be
     * automatically freed when the cancellable is finalized. However the cancellable will block scarce file
     * descriptors until it is finalized if this function is not called. This can cause the application to run out of
     * file descriptors when many cancellables are used at the same time.
     */
    public fun releaseFileDescriptor()

    /**
     * Pops cancellable off the cancellable stack (verifying that cancellable is on the top of the stack).
     */
    public fun popCurrent()

    /**
     * Pushes cancellable onto the cancellable stack. The current cancellable can then be received using [current].
     * This is useful when implementing cancellable operations in code that does not allow you to pass down the
     * cancellable object. The function is typically called automatically by e.g. [io.gitlab.guiVista.io.file.File]
     * operations, so you rarely have to call this yourself.
     */
    public fun pushCurrent()

    /**
     * Resets cancellable to its uncancelled state. If cancellable is currently in use by any cancellable operation
     * then the behavior of this function is undefined. Note that it is generally not a good idea to reuse an existing
     * cancellable for more operations after it has been cancelled once, as this function might tempt you to do. The
     * recommended practice is to drop the reference to a cancellable after cancelling it, and let it die with the
     * outstanding async operations. You should create a fresh cancellable for further async operations.
     */
    public fun reset()

    /**
     * Will set cancellable to cancelled, and will fire the **cancelled** event. (see the warning about race
     * conditions in the documentation for that event if you are planning to connect to it). This function is
     * thread-safe. In other words you can safely call it from a thread other than the one running the operation that
     * was passed the cancellable.
     *
     * The convention within GIO is that cancelling an asynchronous operation causes it to complete asynchronously.
     * That is if you cancel the operation from the same thread in which it is running, then the operation's
     * `GAsyncReadyCallback` will not be invoked until the application returns to the main loop.
     */
    public fun cancel()
}
