package io.gitlab.guiVista.io.file

import io.gitlab.guiVista.core.Closable
import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.io.Cancellable
import io.gitlab.guiVista.io.file.File.Companion.equals
import io.gitlab.guiVista.io.stream.input.FileInputStream

public expect interface FileBase : Closable {
    /**
     * Gets the base name (the last component of the path) for a given [File]. If called for the top level of a system
     * (such as the filesystem root, or a uri like sftp://host/ it will return a single directory separator (and on
     * Windows possibly a drive letter). The base name is a byte string (**not** UTF-8). It has no defined encoding, or
     * rules other than it may not contain zero bytes. If you want to use file names in a user interface you **should**
     * use the display name that you can get by requesting the `G_FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME` attribute, with
     * `queryInfo`.
     *
     * No blocking I/O is done with the property. This property will return the [file's][File] base name, or *""* (an
     * empty String) if the [File] instance is invalid.
     */
    public open val baseName: String

    /**
     * Gets the local pathname for [File] instance if one exists. If it isn't empty then this is guaranteed to be an
     * absolute, canonical path. It might contain symlinks. No blocking I/O is done with the property.
     *
     * This property will return the [file's][File] path, or *""* (an empty String) if no such path exists.
     */
    public open val path: String

    /** Gets the URI for the file. No blocking I/O is done with the property. */
    public open val uri: String

    /**
     * Gets the parent directory for the file. If the file represents the root directory of the file system, then
     * *null* will be returned. This function doesn't do any blocking I/O.
     *
     * Returns a [File] instance to the parent of the given [File], or *null* if there is no parent.
     */
    public open val parent: FileBase?

    /**
     * Exactly like [path] but caches the result via `g_object_set_qdata_full()`. This is useful for example in C
     * applications, which mix g_file_* APIs with native ones. It also avoids an extra duplicated string when possible,
     * so will be generally more efficient. This call does no blocking I/O.
     *
     * Returns a string containing the GFile's path, or *""* if no such path exists. The returned string is owned by
     * [File].
     */
    public open val peekPath: String

    /**
     * Checks to see if the file is native to the platform. A native file is one expressed in the platform-native
     * filename format, e.g. **"C:\Windows"** or **"/usr/bin/"**. This does not mean the file is local, as it might be
     * on a locally mounted remote filesystem. On some systems non-native files may be available using the native
     * filesystem via a userspace filesystem (FUSE). In these cases this call will return *false*, but [path] will
     * still return a native path.
     *
     * This call does no blocking I/O.
     */
    public open val isNative: Boolean

    /**
     * Gets the URI scheme for a GFile. RFC 3986 decodes the scheme as:
     * ```
     * URI = scheme ":" hier-part [ "?" query ] [ "#" fragment ]
     * ```
     * Common schemes include *"file"*, *"http"*, *"ftp"*, etc. This call does no blocking I/O.
     *
     * Returns a String containing the URI scheme for the given [File].
     */
    public open val uriScheme: String

    /**
     * Checks to see if the file has a given [URI scheme][uriScheme]. This call does no blocking I/O.
     * @param uriScheme A string containing a URI scheme.
     * @return A values of *true* if the file's backend supports the given [URI scheme][uriScheme], otherwise *false*
     * if URI scheme is *null*, not supported, or [File] is invalid.
     */
    public open fun hasUriScheme(uriScheme: String): Boolean

    /**
     * Checks if file has a parent, and optionally if it is parent. If parent is *null* then this function returns
     * *true*, if file has any parent at all. If parent is isn't null then *true* is only returned, if file is an
     * immediate child of parent.
     */
    public open fun hasParent(parent: FileBase): Boolean

    /**
     * Gets a child of file with [base name][baseName] equal to [name]. Note that the file with that specific name might not
     * exist, but you can still have a [File] that points to it. You can use this for instance to create that file.
     * This function doesn't do any blocking I/O.
     * @param name The child's base name.
     * @return A [File] to a child specified by [name].
     */
    public open fun fetchChild(name: String): FileBase

    /**
     * Checks whether file has the prefix specified by prefix. In other words if the names of initial elements of
     * file's pathname match prefix. **Only** full pathname elements are matched so a path like /foo isn't considered a
     * prefix of /foobar, only of /foo/bar. A [File] isn't a prefix of itself. If you want to check for equality use
     * [equals].
     *
     * This function doesn't do any I/O, as it works purely on names. As such it can sometimes return *false* even if
     * file is inside a prefix (from a filesystem point of view). Because the prefix of file is an alias of prefix.
     * @param prefix Input [file][File].
     * @return If the files's parent, grandparent, etc is prefix then *true*, otherwise *false*.
     */
    public open fun hasPrefix(prefix: FileBase): Boolean

    /**
     * Gets the parse name of the file. A parse name is a UTF-8 string that describes the file. This is generally used
     * to show the [File] as a nice full path name kind of String in a user interface, like in a location entry. For
     * local files with names that can safely be converted to UTF-8 the pathname is used, otherwise the IRI is used (a
     * form of URI that allows UTF-8 characters unescaped).
     *
     * This function doesn't do any blocking I/O.
     * @return A String containing the [file's][File] parse name.
     */
    public open fun fetchParseName(): String

    /**
     * Gets the path for [descendant] relative to parent (this [File] instance). This function doesn't do any blocking
     * I/O.
     * @param descendant Input [file][File].
     * @return The relative path from [descendant] to parent, or *""* (an empty String) if [descendant] doesn't have a
     * parent as prefix.
     */
    public open fun fetchRelativePath(descendant: FileBase): String

    /**
     * Resolves a relative path for [File] instance to an absolute path. This function doesn't do any blocking I/O.
     * @param relativePath Input [file][File].
     * @return A [File] instance to the resolved path, or *null* if [relativePath] is *null*, or if file is invalid.
     */
    public open fun resolveRelativePath(relativePath: String): FileBase?

    /**
     * Opens a file for reading. The result is a [FileInputStream] that can be used to read the contents of the file.
     * If [cancellable] isn't *null* then the operation can be cancelled by triggering the cancellable object from
     * another thread. However if the operation was cancelled then the error `G_IO_ERROR_CANCELLED` will be returned.
     *
     * If the file doesn't exist then the `G_IO_ERROR_NOT_FOUND` error will be returned. However if the file is a
     * directory then the `G_IO_ERROR_IS_DIRECTORY` error will be returned. Other errors are possible too, and depend
     * on what kind of filesystem the file is on.
     * @param cancellable The [Cancellable] to use or *null*.
     * @param error The [Error] to use or *null*.
     * @return A [FileInputStream] object or *null* on error. Use the [FileInputStream.close] function when finished
     * with the object
     */
    public open fun read(cancellable: Cancellable? = null, error: Error? = null): FileInputStream?

    /**
     * Deletes a file. If the file is a directory, it will only be deleted if it is empty. This has the same semantics
     * as `g_unlink()`. However if the file doesn't exist then `G_IO_ERROR_NOT_FOUND` will be returned.
     *
     * If cancellable isn't *null* then the operation can be cancelled by triggering the cancellable object from
     * another thread. If the operation was cancelled then the error `G_IO_ERROR_CANCELLED` will be returned.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return A value of *true* if the file was deleted.
     */
    public open fun delete(cancellable: Cancellable? = null, error: Error? = null): Boolean

    /**
     * Sends file to the **"Trashcan"**, if possible. This is similar to deleting it, but the user can recover it
     * before emptying the trashcan. Not all file systems support trashing, so this call can return the
     * `G_IO_ERROR_NOT_SUPPORTED` error. Since GLib 2.66, the **x-gvfs-notrash** unix mount option can be used to
     * disable [moveToTrash] support for certain mounts, the `G_IO_ERROR_NOT_SUPPORTED` error will be returned in that
     * case.
     *
     * If cancellable isn't *null* then the operation can be cancelled by triggering the cancellable object from
     * another thread. If the operation was cancelled then the error `G_IO_ERROR_CANCELLED` will be returned.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return A value of *true* on successfully moving the file to the **"Trashcan"**.
     */
    public open fun moveToTrash(cancellable: Cancellable? = null, error: Error? = null): Boolean
}
