package io.gitlab.guiVista.io.file

import io.gitlab.guiVista.core.ObjectBase

/** Provides functionality for completing file names. */
public expect class FileNameCompleter : ObjectBase {
    public companion object {
        public fun create(): FileNameCompleter
    }

    /**
     * Obtains a completion for [initialText] from the completer.
     * @param initialText The text to be completed.
     * @return A completed string, or *""* (an empty String) if no completion exists.
     */
    public fun fetchCompletionSuffix(initialText: String): String

    /**
     * Gets an array of completion strings for a given [initial text][initialText].
     * @param initialText The text to be completed.
     * @return array of strings with possible completions for [initialText]. This array must be freed by g_strfreev() when finished..
     */
    public fun fetchCompletions(initialText: String): Array<String>

    /**
     * If [dirsOnly] is *true* then the completer will only complete directory names, and not file names.
     * @param dirsOnly Is set to *true* for directory names only.
     */
    public fun changeDirsOnly(dirsOnly: Boolean)
}