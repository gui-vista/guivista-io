package io.gitlab.guiVista.io.file

public object FileNameCompleterEvent {
    public const val gotCompletionData: String = "got-completion-data"
}
