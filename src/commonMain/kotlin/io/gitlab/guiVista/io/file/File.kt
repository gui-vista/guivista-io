package io.gitlab.guiVista.io.file

/** File and Directory Handling. */
public expect class File : FileBase {
    public companion object {
        /**
         * Constructs a [File] for a given path. This operation never fails, but the returned object might not support
         * any I/O operation **if** path is malformed.
         * @param path A string containing a relative, or absolute path. The string **must** be encoded in the glib
         * filename encoding.
         * @return A new [File] for the given path.
         */
        public fun fromPath(path: String): File

        /**
         * Constructs a [File] for a given URI. This operation never fails, but the returned object might not support
         * any I/O operation **if** uri is malformed, or **if** the uri type isn't supported.
         * @param uri A UTF-8 encoded String containing a URI.
         * @return A new [File] for the given uri.
         */
        public fun fromUri(uri: String): File

        /**
         * Constructs a [File] with the given [parseName] (i.e. something given by `g_file_get_parse_name()`). This
         * operation never fails, but the returned object might not support any I/O operation **if** [parseName] cannot
         * be parsed.
         * @param parseName A file name, or path to be parsed.
         * @return A new [File] instance.
         */
        public fun parseName(parseName: String): File

        /**
         * Constructs a [File] with the given argument from the command line. The value of [arg] can be either be a
         * URI, an absolute path, or a relative path resolved relative to the current working directory. This operation
         * never fails, but the returned object might not support any I/O operation if [arg] points to a malformed path.
         *
         * If [cwd] is specified then the current working directory is included instead of using the current working
         * directory of the process. This function combines [g_file_new_for_commandline_arg](https://developer.gnome.org/gio/stable/GFile.html#g-file-new-for-commandline-arg),
         * and [g_file_new_for_commandline_arg_and_cwd](https://developer.gnome.org/gio/stable/GFile.html#g-file-new-for-commandline-arg-and-cwd) functions into a single function.
         * @param arg A command line String.
         * @param cwd The current working directory to use.
         * @return A new [File] instance.
         */
        public fun fromCommandLine(arg: String, cwd: String = ""): File
    }

    /**
     * Creates a hash value for the [File] instance. This function doesn't do any blocking I/O.
     * @return If [File] instance isn't valid then return *0*, otherwise an Int that can be used as hash value for the
     * [File] instance. This function is intended for easily hashing a [File] to add to a GHashTable, or similar data
     * structure.
     */
    override fun hashCode(): Int

    /**
     * Checks if the two given files refer to the same file. Note that two files that differ can still refer to the
     * same file on the filesystem due to various forms of filename aliasing. This call does no blocking I/O.
     */
    override fun equals(other: Any?): Boolean
}
