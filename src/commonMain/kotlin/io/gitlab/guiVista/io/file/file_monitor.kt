package io.gitlab.guiVista.io.file

import io.gitlab.guiVista.core.ObjectBase

/** Monitors a file or directory for changes. */
public expect class FileMonitor : ObjectBase {
    /**
     * Will be *true* if the file monitor was cancelled.
     *
     * Data binding property name: **cancelled**
     */
    public val isCancelled: Boolean

    /** Cancels a file monitor. */
    public fun cancel()

    /**
     * Sets the rate limit to which the [FileMonitor] will report consecutive change events to the same file.
     * @param limit A limit in milli seconds to poll for changes.
     */
    public fun changeRateLimit(limit: UInt)
}
