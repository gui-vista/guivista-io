package io.gitlab.guiVista.io.application

/** Core application class. */
public expect class Application : ApplicationBase {
    public companion object {
        public fun create(id: String = "org.example.app", flags: UInt = 0u): Application
    }
}
