package io.gitlab.guiVista.io.application.action

/** A simple [ActionGroupBase] implementation */
public expect class SimpleActionGroup : ActionGroupBase, ActionMap
