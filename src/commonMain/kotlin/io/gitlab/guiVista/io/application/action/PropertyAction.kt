package io.gitlab.guiVista.io.application.action

import io.gitlab.guiVista.core.ObjectBase

/** An action reflecting on a GObject property. */
public expect class PropertyAction : Action, ObjectBase
