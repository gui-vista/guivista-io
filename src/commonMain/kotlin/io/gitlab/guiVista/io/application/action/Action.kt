package io.gitlab.guiVista.io.application.action

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.dataType.variant.Variant
import io.gitlab.guiVista.core.dataType.variant.VariantType

/** Represents a single named action that may have state. */
public expect interface Action {
    /** The name of the action. */
    public open val name: String

    /** The parameter type. */
    public open val paramType: VariantType?

    /** The state type if the action is stateful, otherwise *null* is returned. */
    public open val stateType: VariantType?

    /**
     * The state range hint. Will return *null* if the action isn't stateful, or there is no hint about the valid range
     * of values for the state of the action.
     */
    public open val stateHint: Variant?

    /**
     * If action is currently enabled. In the case of the action being disabled all calls to `g_action_activate()`, and
     * [changeState] have no effect. Default value is *true*.
     */
    public open val enabled: Boolean

    /**
     * Request for the state of [Action] to be changed to value. The action must be stateful, and value **MUST** be of
     * the correct type. See g_action_get_state_type(). This call merely requests a change. The action may refuse to
     * change its state or may change its state to something other than value.
     *
     * If [value] is floating then it is consumed.
     * @param value The new state.
     * @see stateHint
     * @see stateType
     */
    public open fun changeState(value: Variant)

    /**
     * Queries the current state of action. If the action is not stateful then *null* will be returned. However if the
     * action is stateful then the type of the return value is the type given by [stateType]. The return value
     * (if non null) should be freed with [Variant.close] when it is no longer required.
     */
    public open fun fetchState(): Variant?

    /**
     * Activates the action. Note that [actionParam] **MUST** be the correct type of parameter for the action
     * (ie: the parameter type given at construction time). If the parameter type was *null* then [actionParam]
     * must also be *null*.
     *
     * If [actionParam] is floating then it is consumed.
     * @param actionParam The parameter for the activation.
     */
    public open fun activate(actionParam: Variant?)

    /**
     * Parses a detailed action name into its separate name and target components. Detailed action names can have three
     * formats. First format is used to represent an action name with no target value, and consists of just an action
     * name containing no whitespace nor the characters ':', '(' or ')'. For example: "app.action". Second format is
     * used to represent an action with a target value that is a non-empty string consisting only of alphanumerics,
     * plus '-' and '.'. In that case the action name, and target value are separated by a double colon ("::"). For
     * example: "app.action::target". Third format is used to represent an action with any type of target value,
     * including strings. The target value follows the action name, surrounded in parens. For example:
     * "app.action(42)". If a tuple-typed value is desired, it must be specified in the same way, resulting in two sets
     * of parens, for example: "app.action((1,2,3))". A string target can be specified this way as well:
     * "app.action('target')". For strings, this third format must be used if * target value is empty or contains
     * characters other than alphanumerics, **-** and **.**.
     * @param detailedName A detailed action name.
     * @param actionName The action name.
     * @param targetValue The target value, or *null* for no target.
     * @param error The error to use, or *null* to not use it.
     * @return A value of *true* if successful, else *false* with [error] set.
     */
    public open fun parseDetailedName(
        detailedName: String,
        actionName: String,
        targetValue: Variant?,
        error: Error? = null
    ): Boolean

    /**
     * Formats a detailed action name from [actionName] and [targetValue]. It is an error to call this function with
     * an invalid [action name][actionName]. This function is the opposite of [parseDetailedName]. It will produce a
     * String that can be parsed back to the [actionName] and [targetValue] by that function.
     * @param actionName A valid action name.
     * @param targetValue A target value, or *null*.
     * @return A detailed format String.
     * @see parseDetailedName
     */
    public open fun printDetailedName(actionName: String, targetValue: Variant?): String
}
