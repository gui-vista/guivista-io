package io.gitlab.guiVista.io.application.menu

import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.dataType.variant.Variant
import io.gitlab.guiVista.core.dataType.variant.VariantType

/** Represents the contents of a menu. */
public expect interface MenuModelBase : ObjectBase {
    /** Queries if model is mutable. */
    public open val isMutable: Boolean

    /** The number of items in model. */
    public open val totalItems: Int

    /**
     * Queries the item at position [itemIndex] in model for the attribute specified by [attrib]. If [expectedType] is
     * non null then it specifies the expected type of the attribute. If it is *null* then any type will be accepted.
     * If the attribute exists, and matches [expectedType] (or if the expected type is unspecified) then the value is
     * returned. However if the attribute doesn't exist, or doesn't match the expected type then *null* is returned.
     * @param itemIndex The index of the item.
     * @param attrib The attribute to query.
     * @param expectedType The expected type of the attribute, or *null*.
     * @return The value of the attribute.
     */
    public open fun fetchItemAttributeValue(itemIndex: Int, attrib: String, expectedType: VariantType?): Variant?

    /**
     * Queries item at position [itemIndex] in model for the attribute specified by [attrib]. If the attribute
     * exists, and matches the [VariantType] corresponding to [formatStr] then [formatStr] is used to deconstruct the
     * value into the positional parameters and *true* is returned. However if the attribute doesn't exist, or it
     * doesn't but has the wrong type, then the positional parameters are ignored and *false* is returned.
     *
     * This function is a mix of [fetchItemAttributeValue], and `g_variant_get()`, followed by a [Variant.close]. As
     * such [formatStr] must make a complete copy of the data (since the [Variant] may go away after the call to
     * [Variant.close]). In particular, no **&** characters are allowed in [formatStr].
     * @param itemIndex The index of the item.
     * @param attrib The attribute ot query.
     * @param formatStr A [Variant] format String.
     */
    public open fun fetchItemAttribute(itemIndex: Int, attrib: String, formatStr: String): Boolean

    /**
     * Queries the item at position [itemIndex] in model for the link specified by [link]. If the link exists then the
     * linked [MenuModelBase] is returned. If the link doesn't exist then *null* is returned.
     * @param itemIndex The index of the item.
     * @param link The link to the query.
     * @return The linked [MenuModelBase] or *null*.
     */
    public open fun fetchItemLink(itemIndex: Int, link: String): MenuModelBase?

    /**
     * Creates a [MenuAttributeIterator] to iterate over the attributes of the item at position [itemIndex] in the
     * model. You **MUST** free the iterator with [MenuAttributeIterator.close] when you are done.
     * @param itemIndex The index of the item.
     * @return A new [MenuAttributeIterator].
     */
    public open fun iterateItemAttributes(itemIndex: Int): MenuAttributeIterator

    /**
     * Creates a [MenuLinkIterator] to iterate over the links of the item at position [itemIndex] in model. You
     * **MUST** free the iterator with [MenuLinkIterator.close] when you are done.
     * @param itemIndex The index of the item.
     * @return A new [MenuLinkIterator].
     */
    public open fun iterateItemLinks(itemIndex: Int): MenuLinkIterator
}
