package io.gitlab.guiVista.io.application.action

public object SimpleActionEvent {
    public const val activate: String = "activate"
    public const val changeState: String = "change-state"
}
