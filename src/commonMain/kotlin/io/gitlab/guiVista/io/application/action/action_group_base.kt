package io.gitlab.guiVista.io.application.action

import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.dataType.variant.Variant
import io.gitlab.guiVista.core.dataType.variant.VariantType

/** A group of actions. */
public expect interface ActionGroupBase : ObjectBase {
    /**
     * Lists the actions contained within [ActionGroupBase].
     * @return An array of the names of the actions in the group.
     */
    public open fun listActions(): Array<String>

    /**
     * Queries all aspects of the named action within an [ActionGroupBase]. This function acquires the information
     * available from [hasAction], [fetchActionEnabled], [fetchActionParameterType], [fetchActionStateType],
     * [fetchActionStateHint], and [fetchActionState] with a single function call. This provides two main benefits.
     *
     * The first is the improvement in efficiency that comes with not having to perform repeated lookups of the action
     * in order to discover different things about it. The second is that implementing [ActionGroupBase] can now be
     * done by only overriding this function. The interface provides a default implementation of this function that
     * calls the individual functions, as required to fetch the information. The interface also provides default
     * implementations of those functions that call this function. All implementations **MUST** override either this
     * function, or all of the others.
     *
     * If the action exists then *true* is returned and any of the requested fields (as indicated by having a non null
     * reference passed in) are filled. However if the action doesn't exist then *false* is returned, and the fields
     * may or may not have been modified.
     * @param actionName The name of the action in the group.
     * @param enabled If the action is presently enabled.
     * @param paramType The parameter type, or *null* if none needed.
     * @param stateType The state type, or *null* if stateless..
     * @param stateHint The state hint, or *null* if none.
     * @param state The current state, or *null* if stateless.
     * @return A value of *true* if the action exists, otherwise *false*.
     */
    public open fun queryAction(
        actionName: String,
        enabled: Boolean,
        paramType: VariantType?,
        stateType: VariantType?,
        stateHint: Variant,
        state: Variant
    ): Boolean

    /**
     * Checks if the named action exists within [ActionGroupBase].
     * @param actionName The name of the action to check for.
     * @return Whether the named action exists.
     */
    public open fun hasAction(actionName: String): Boolean

    /**
     * Checks if the named action within [ActionGroupBase] is currently enabled. An action **MUST** be enabled in order
     * to be activated, or in order to have its state changed from outside callers.
     * @param actionName The name of the action to query.
     * @return Whether or not the action is currently enabled.
     */
    public open fun fetchActionEnabled(actionName: String): Boolean

    /**
     * Queries the type of the parameter that must be given when activating the named action within [ActionGroupBase].
     * When activating the action using [activateAction], the [Variant] given to that function must be of the type
     * returned by this function. In the case that this function returns *null*, you must not give any [Variant], but
     * *null* instead.
     *
     * The parameter type of a particular action will never change, but it is possible for an action to be removed, and
     * for a new action to be added with the same name but a different parameter type.
     * @param actionName The name of the action to query.
     * @return The parameter type.
     */
    public open fun fetchActionParameterType(actionName: String): VariantType?

    /**
     * Queries the type of the state of the named action within [ActionGroupBase]. If the action is stateful then this
     * function returns the [VariantType] of the state. All calls to [changeActionState] **MUST** give a [Variant] of
     * this type, and [fetchActionState] will return a [Variant] of the same type. If the action is not stateful then
     * this function will return *null*. In that case, [fetchActionState] will return *null*, and you must not call
     * [changeActionState].
     *
     * The state type of a particular action will never change, but it is possible for an action to be removed, and for
     * a new action to be added with the same name but a different state type.
     * @param actionName The name of the action to query.
     * @return The state type, if the action is stateful.
     */
    public open fun fetchActionStateType(actionName: String): VariantType?

    /**
     * Requests a hint about the valid range of values for the state of the named action within [ActionGroupBase]. If
     * *null* is returned it either means that the action is not stateful, or that there is no hint about the valid
     * range of values for the state of the action. If a [Variant] array is returned then each item in the array is a
     * possible value for the state. If a [Variant] pair (ie: two-tuple) is returned then the tuple specifies the
     * inclusive lower, and upper bound of valid values for the state.
     *
     * In any case the information is merely a hint. It may be possible to have a state value outside of the hinted
     * range, and setting a value within the range may fail. The return value (if non null) should be freed with
     * [Variant.close] when it is no longer required.
     * @param actionName The name of the action to query.
     * @return The state range hint
     */
    public open fun fetchActionStateHint(actionName: String): Variant?

    /**
     * Queries the current state of the named action within [ActionGroupBase]. If the action is not stateful then
     * *null* will be returned. If the action is stateful then the type of the return value is the type given by
     * [fetchActionStateType]. The return value (if non null) should be freed with [Variant.close] when it is no
     * longer required.
     * @param actionName The name of the action to query.
     * @return The current state of the action.
     */
    public open fun fetchActionState(actionName: String): Variant?

    /**
     * Request for the state of the named action within [ActionGroupBase] to be changed to [value]. The action
     * **MUST** be stateful, and [value] **MUST** be of the correct type. This call merely requests a change. The
     * action may refuse to change its state, or may change its state to something other than [value]. If the value
     * [Variant] is floating then it is consumed.
     * @param actionName The name of the action to request the change on.
     * @param value The new state.
     * @see fetchActionStateType
     * @see fetchActionStateHint
     */
    public open fun changeActionState(actionName: String, value: Variant)

    /**
     * Activate the named action within [ActionGroupBase]. If the action is expecting a parameter then the correct
     * type of parameter **MUST** be given as parameter. If the action is expecting no parameters then [param]
     * **MUST** be *null*.
     * @See fetchActionParameterType
     */
    public open fun activateAction(actionName: String, param: Variant?)
}
