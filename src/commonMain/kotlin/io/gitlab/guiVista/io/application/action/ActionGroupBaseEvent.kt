package io.gitlab.guiVista.io.application.action

public object ActionGroupBaseEvent {
    public val actionAdded: String = "action-added"
    public val actionEnabledChanged: String = "action-enabled-changed"
    public val actionRemoved: String = "action-removed"
    public val actionStateChanged: String = "action-state-changed"
}
