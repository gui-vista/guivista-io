package io.gitlab.guiVista.io.application.menu

public object MenuModelBaseEvent {
    public const val itemsChanged: String = "item-changed"
}
