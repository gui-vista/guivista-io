package io.gitlab.guiVista.io.application.action

/** An interface for action containers. */
public expect interface ActionMap
