package io.gitlab.guiVista.io.application

public object ApplicationBaseEvent {
    public const val activate: String = "activate"
    public const val startup: String = "startup"
    public const val shutdown: String = "shutdown"
    public const val open: String = "open"
}
