package io.gitlab.guiVista.io.application

import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.io.application.action.ActionGroupBase
import io.gitlab.guiVista.io.application.action.ActionMap
import io.gitlab.guiVista.io.file.File

/** Base interface for application objects. */
public expect interface ApplicationBase : ActionMap, ActionGroupBase, ObjectBase {
    /**
     * The flags for the application. Note that this property can only be modified if the application has not yet been
     * registered.
     *
     * Data binding property name: **flags**
     */
    public open var flags: UInt

    /**
     * The unique identifier for the application. Default value is *""* (an empty String).
     *
     * Data binding property name: **application-id**
     */
    public open val appId: String

    /**
     * Time (in ms) to stay alive after becoming idle. Default value is *0*.
     *
     *  Data binding property name: **inactivity-timeout**
     */
    public open var inactivityTimeout: UInt

    /**
     * The base resource path for the application. Default value is *""* (an empty String).
     *
     * Data binding property name: **resource-base-path**
     */
    public open var resourceBasePath: String

    /**
     * Whether the application is currently marked as busy through `g_application_mark_busy()`, or
     * `g_application_bind_busy_property()`. Default value is *false*.
     *
     * Data binding property name: **is-busy**
     */
    public open val isBusy: Boolean

    /**
     * If `g_application_register()` has been called. Default value is *false*.
     *
     * Data binding property name: **is-registered**
     */
    public open val isRegistered: Boolean

    /**
     * If this application instance is remote. Default value is *false*.
     *
     * Data binding property name: **is-remote**
     */
    public open val isRemote: Boolean

    /**
     * Increases the use count of the application. Use this function to indicate that the application has a reason to
     * continue to run. For example, [hold] is called by GTK+ when a top level window is on the screen. To cancel the
     * hold call [release].
     */
    public open fun hold()

    /**
     * Decrease the use count of the application. When the use count reaches *0* the application will stop running.
     * **Never** call this function except to cancel the effect of a previous call to [hold].
     */
    public open fun release()

    /**
     * Immediately quits the application. Upon return to the main loop, [run] will return, calling only the 'shutdown'
     * function before doing so. The hold count is ignored. Take care if your code has called [hold] on the
     * application, and is therefore still expecting it to exist. **Note** that you may have called [hold] indirectly,
     * for example through `gtk_application_add_window`.
     *
     * The result of calling [run] again after it returns is unspecified.
     */
    public open fun quit()

    /**
     * Opens the given files. In essence this results in the **open** event being emitted in the primary instance. The
     * [hint] is simply passed through to the **open** event. It is intended to be used by applications that have
     * multiple modes for opening files (eg: *view* vs *edit*, etc). Unless you have a need for this functionality you
     * should not set the [hint] parameter.
     *
     * The application **MUST** be registered before calling this function, and it **MUST** have the
     * `G_APPLICATION_HANDLES_OPEN` flag set.
     * @param files An array of files to open.
     * @param hint The hint to use.
     */
    public open fun open(files: Array<File>, hint: String = "")

    /**
     * Increases the busy count of the application. Use this function to indicate that the application is busy, for
     * instance while a long running operation is pending. The busy state will be exposed to other processes, so a
     * session shell will use that information to indicate the state to the user (e.g. with a spinner).
     *
     * To cancel the busy indication use [unmarkBusy].
     */
    public open fun markBusy()

    /**
     * Decreases the busy count of the application. When the busy count reaches *0* the new state will be propagated to
     * other processes. This function **MUST** only be called to cancel the effect of a previous call to [markBusy].
     */
    public open fun unmarkBusy()

    /**
     * Runs the application.
     * @return A status code is returned when the application exits. Any code not equal to *0* means a error has
     * occurred.
     */
    public open fun run(): Int

    /**
     * Sends a [notification] on behalf of application to the desktop shell. There is no guarantee that the
     * notification is displayed immediately, or even at all. Notifications **may** persist after the application
     * exits. It will be D-Bus-activated when the [notification], or one of its actions is activated. Modifying
     * [notification] after this call has no effect. However the object can be reused for a later call to this function.
     *
     * The [id] may be any string that uniquely identifies the event for the [application][ApplicationBase]. It does
     * not need to be in any special format. For example, "new-message" might be appropriate for a notification about
     * new messages. If a previous notification was sent with the same [id] it will be replaced with [notification],
     * and shown again as if it was a new notification. This works even for notifications sent from a previous
     * execution of the [application][ApplicationBase], as long as [id] is the same string.
     *
     * Note that [id] may be *""* (an empty String), but it is impossible to replace, or withdraw notifications without
     * an id. If [notification] is no longer relevant it can be withdrawn with [withdrawNotification].
     * @param id The ID of the [application][ApplicationBase], or *""* (an empty String).
     * @param notification The notification to send.
     */
    public open fun sendNotification(id: String = "", notification: Notification)

    /**
     * Withdraws a notification that was sent with [sendNotification]. This call does nothing if a notification with
     * [id] doesn't exist, or the notification was never sent. This function works even for notifications sent in
     * previous executions of this [application][ApplicationBase], as long as [id] is the same as it was for the sent
     * notification.
     *
     * Note that notifications are dismissed when the user clicks on one of the buttons in a notification, or triggers
     * its default action, so there is no need to explicitly withdraw the notification in that case.
     * @param id The ID of the previously sent notification..
     */
    public open fun withdrawNotification(id: String)

    public companion object {
        /**
         * Checks if [id] is a valid application identifier. A valid ID is required for calls to
         * `Application()`, and [appId]. All application identifiers follow the same
         * format as D-Bus well-known bus names. For convenience the restrictions on application identifiers are
         * reproduced here:
         *
         * - Application identifiers are composed of 1 or more elements separated by a period (.) character. All elements must contain at least one character.
         * - Each element must only contain the ASCII characters [A-Z][a-z][0-9]_-, with - discouraged in new application identifiers. Each element must not begin with a digit.
         * - Application identifiers must contain at least one . (period) character (and thus at least two elements).
         * - Application identifiers must not begin with a . (period) character.
         * - Application identifiers must not exceed 255 characters.
         * @param id A potential application identifier.
         * @return A value of *true* if [id] is valid.
         */
        public fun validAppId(id: String): Boolean
    }
}
