package io.gitlab.guiVista.io.application.action

/** Default implementation of [ActionGroupBase]. */
public expect class ActionGroup : ActionGroupBase
