package io.gitlab.guiVista.io.stream.input

/** Default implementation of [BufferedInputStreamBase]. */
public expect class BufferedInputStream : BufferedInputStreamBase {
    public companion object {
        /**
         * Creates a new [BufferedInputStream] from the given [baseStream], with a buffer set to [size], or the buffer
         * set to a default size (4 KB) if [size] is *0*.
         * @param baseStream The input stream to use.
         * @param size The size of the buffer.
         * @return A new [BufferedInputStream] instance for the given [baseStream].
         */
        public fun create(baseStream: InputStreamBase, size: ULong = 0uL): BufferedInputStream
    }
}
