package io.gitlab.guiVista.io.stream.input

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.io.Cancellable

/** Provides buffered reads. */
public expect interface BufferedInputStreamBase : FilterInputStream {
    /**
     * The size of the input buffer.
     *
     * Data binding property name: **buffer-size**
     */
    public open var bufferSize: ULong

    /** The size of the available data within the stream. */
    public open val availableDataSize: ULong

    /**
     * Returns the buffer with the currently available bytes. The returned buffer must **NOT** be modified, and will
     * become invalid when reading from the stream or filling the buffer.
     * @return A Pair containing the following:
     * 1. count - The number of bytes available in the buffer.
     * 2. buffer - Holds all read data (is read only).
     */
    public open fun peek(): Pair<ULong, ByteArray>

    /**
     * Peeks in the buffer, copying data of size [count] into buffer, with an [offset].
     * @param offset The offset from the data.
     * @param count The size of the data.
     */
    public open fun peekAndCopy(buffer: ByteArray, count: ULong, offset: ULong)

    /**
     * Tries to read [count] bytes from the stream into the buffer. Will block during this read. If [count] is *0*
     * then this function returns *0* and does nothing. A value of [count] larger than `G_MAXSSIZE` will cause a
     * `G_IO_ERROR_INVALID_ARGUMENT` error. On success the number of bytes read into the buffer is returned. It is not
     * an error if this is not the same as the requested size, as it can happen e.g. near the end of a file. Zero is
     * returned on end of file (or if [count] is zero), but never otherwise.
     *
     * If [count] is *-1* then the attempted read size is equal to the number of bytes that are required to fill the
     * buffer. If [cancellable] isn't *null* then the operation can be cancelled by triggering the cancellable object
     * from another thread. If the operation was cancelled then the error `G_IO_ERROR_CANCELLED` will be returned. If
     * an operation was partially finished when the operation was cancelled the partial result will be returned,
     * without an error.
     *
     * On error *-1* is returned, and [error] is set accordingly. For the asynchronous, non-blocking, version of this
     * function, see `g_buffered_input_stream_fill_async()`.
     * @param count The number of bytes that will be read from the stream.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return The number of bytes read into stream's buffer, up to [count], or *-1* on error.
     */
    public open fun fill(count: Long, cancellable: Cancellable? = null, error: Error? = null): Long

    /**
     * Tries to read a single byte from the stream or the buffer. Will block during this read. On success the byte
     * read from the stream is returned. On end of stream *-1* is returned but it's not an exceptional error, and
     * [error] isn't set. If [cancellable] isn't *null* then the operation can be cancelled by triggering the
     * cancellable object from another thread. If the operation was cancelled then the error `G_IO_ERROR_CANCELLED`
     * will be returned. If an operation was partially finished when the operation was cancelled the partial result
     * will be returned, without an error.
     *
     * On error *-1* is returned, and [error] is set accordingly.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return The byte read from the stream, or *-1* on end of stream or error.
     */
    public open fun readByte(cancellable: Cancellable? = null, error: Error? = null): Int
}