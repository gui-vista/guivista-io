package io.gitlab.guiVista.io.stream.input

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.io.Cancellable
import io.gitlab.guiVista.io.file.FileInfo
import io.gitlab.guiVista.io.stream.Seekable

/** File input streaming operations. */
public expect class FileInputStream : InputStreamBase, Seekable {
    /**
     * Queries a file input stream the given [attributes]. This function blocks while querying the stream. For the
     * asynchronous (non-blocking) version of this function, see `g_file_input_stream_query_info_async()`. While the
     * stream is blocked, the stream will set the pending flag internally, and any other operations on the stream will
     * fail with `G_IO_ERROR_PENDING`.
     * @param attributes A file attribute query [String].
     * @param cancellable The [Cancellable] to use or *null*.
     * @param error The [Error] to use or *null*.
     * @return A instance of [FileInfo] or *null* on error.
     */
    public fun queryInfo(attributes: String, cancellable: Cancellable? = null, error: Error? = null): FileInfo?
}
