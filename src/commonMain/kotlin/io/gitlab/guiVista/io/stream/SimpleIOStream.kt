package io.gitlab.guiVista.io.stream

import io.gitlab.guiVista.io.stream.input.InputStreamBase
import io.gitlab.guiVista.io.stream.output.OutputStreamBase

/** A wrapper around an input, and output stream. */
public expect class SimpleIOStream : IOStreamBase {
    public companion object {
        /**
         * Creates a new [SimpleIOStream] wrapping [inputStream] and [outputStream].
         * @param inputStream The input stream to use.
         * @param outputStream The output stream to use.
         * @return A new instance of [SimpleIOStream].
         * @see IOStreamBase
         */
        public fun create(inputStream: InputStreamBase, outputStream: OutputStreamBase): SimpleIOStream
    }
}
