package io.gitlab.guiVista.io.stream

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.io.Cancellable
import io.gitlab.guiVista.io.stream.input.InputStreamBase
import io.gitlab.guiVista.io.stream.output.OutputStreamBase

/** Base interface for implementing read/write streams. */
public expect interface IOStreamBase : ObjectBase {
    /**
     * The input stream for this object. This is used for reading.
     *
     * Data binding property name: **input-stream**
     */
    public open val inputStream: InputStreamBase

    /**
     * The output stream for this object. This is used for writing.
     *
     * Data binding property name: **output-stream**
     */
    public open val outputStream: OutputStreamBase

    /**
     * Checks if a stream is closed.
     *
     * Data binding property name: **closed**
     */
    public open val streamIsClosed: Boolean

    /** Checks if a stream has pending actions. */
    public open val hasPending: Boolean

    /**
     * Sets stream to have actions pending. If the pending flag is already set or stream is closed, it will return
     * *false* and set [error].
     * @param error The [Error] to use or *null*.
     * @return A value of *true* if pending was previously unset and is now set.
     */
    public open fun changePending(error: Error? = null): Boolean

    /** Clears the pending flag on the stream. */
    public open fun clearPending()

    /**
     * Closes the stream, releasing resources related to it. This will also close the individual input and output
     * streams, if they are not already closed. Once the stream is closed all other operations will return
     * `G_IO_ERROR_CLOSED`. Closing a stream multiple times will not return an error. Closing a stream will
     * automatically flush any outstanding buffers in the stream.
     *
     * Streams will be automatically closed when the last reference is dropped, but you might want to call this
     * function to make sure resources are released as early as possible. Some streams might keep the backing store of
     * the stream (e.g. a file descriptor) open after the stream is closed. See the documentation for the individual
     * stream for details. On failure the first error that happened will be reported, but the close operation will
     * finish as much as possible. A stream that failed to close will still return `G_IO_ERROR_CLOSED` for all
     * operations. Still it is important to check and report the error to the user, otherwise there might be a loss of
     * data as all data might not be written.
     *
     * If cancellable isn't *null* then the operation can be cancelled by triggering the cancellable object from
     * another thread. If the operation was cancelled then the error `G_IO_ERROR_CANCELLED` will be returned.
     * Cancelling a close will still leave the stream closed, but some streams can use a faster close that doesn't
     * block to e.g. check errors.
     *
     * The default implementation of this method just calls close on the individual input/output streams.
     * @param cancellable The [Cancellable] to use or *null*.
     * @param error The [Error] to use or *null*.
     * @return A value of *true* on success, *false* on failure.
     */
    public open fun closeStream(cancellable: Cancellable? = null, error: Error? = null): Boolean
}
