package io.gitlab.guiVista.io.stream.output

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.io.Cancellable

/** Interface for pollable output streams. */
public expect interface PollableOutputStream {
    /**
     * Checks if stream is actually pollable. Some classes may implement [PollableOutputStream] but have only certain
     * instances of that class be pollable. If this property is *false* then the behavior of other
     * [PollableOutputStream] functions/properties is undefined. For any given stream, the value returned by this
     * property is constant; a stream cannot switch from pollable to non-pollable or vice versa.
     */
    public open val canPoll: Boolean

    /**
     * Checks if stream can be written. Note that some stream types may not be able to implement this 100% reliably,
     * and it is possible that a call to [io.gitlab.guiVista.io.stream.output.OutputStreamBase.write] after this
     * returns *true* would still block. To guarantee non-blocking behavior, you should always use
     * [writeNonBlocking], which will return a `G_IO_ERROR_WOULD_BLOCK` error rather than blocking.
     */
    public open val isWritable: Boolean

    /**
     * Attempts to write up to count bytes from the [buffer] to the stream, as with
     * [io.gitlab.guiVista.io.stream.output.OutputStreamBase.write]. If the stream is not currently writable then this
     * function will immediately return `G_IO_ERROR_WOULD_BLOCK`, and you can use
     * `g_pollable_output_stream_create_source()` to create a `GSource` that will be triggered when stream is writable.
     *
     * Note that since this function never blocks, you cannot actually use [cancellable] to cancel it. However it this
     * function return an error if [cancellable] has already been cancelled when you call, which may happen if you
     * call this function after a source triggers due to having been cancelled. Also note that if
     * `G_IO_ERROR_WOULD_BLOCK` is returned some underlying transports like D/TLS require that you re-send the same
     * buffer and count in the next write call.
     * @param buffer The buffer to write data from.
     * @param cancellable The [Cancellable] to use or *null*.
     * @param error The [Error] to use or *null*.
     * @return The number of bytes written, or *-1* on error (including `G_IO_ERROR_WOULD_BLOCK`).
     */
    public open fun writeNonBlocking(buffer: ByteArray, cancellable: Cancellable? = null, error: Error? = null): Long
}
