package io.gitlab.guiVista.io.stream.input

/**
 * Base interface for input stream implementations that perform some kind of filtering operation on a base stream.
 * Typical examples of filtering operations are character set conversion, compression, and byte order flipping.
 */
public expect interface FilterInputStream : InputStreamBase {
    /** The base stream for the filter stream. */
    public val baseStream: InputStreamBase

    /** Whether the base stream will be closed when stream is closed. */
    public var closeBaseStream: Boolean
}
