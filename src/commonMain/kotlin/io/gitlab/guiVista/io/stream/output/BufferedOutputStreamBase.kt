package io.gitlab.guiVista.io.stream.output

/** Provides buffered writes. */
public expect interface BufferedOutputStreamBase {
    /**
     * The size of the buffer in the stream.
     *
     * Data binding property name: **buffer-size**
     */
    public open val bufferSize: ULong

    /**
     * Checks if the buffer automatically grows as data is added. If this property is *true* then each write will just
     * make the buffer larger, and you **MUST** manually flush the buffer to actually write out the data to the
     * underlying stream.
     *
     * Data binding property name: **auto-grow**
     */
    public open var autoGrow: Boolean
}
