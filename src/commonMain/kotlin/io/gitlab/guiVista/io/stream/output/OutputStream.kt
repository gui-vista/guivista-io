package io.gitlab.guiVista.io.stream.output

/** Default implementation of [OutputStreamBase]. */
public expect class OutputStream : OutputStreamBase
