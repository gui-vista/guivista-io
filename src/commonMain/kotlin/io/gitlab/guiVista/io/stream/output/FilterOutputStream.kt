package io.gitlab.guiVista.io.stream.output

/**
 * Base interface for output stream implementations that perform some kind of filtering operation on a base stream.
 * Typical examples of filtering operations are character set conversion, compression, and byte order flipping.
 */
public expect interface FilterOutputStream : OutputStreamBase {
    /** The base stream for the filter stream. */
    public val baseStream: OutputStreamBase

    /** Whether the base stream will be closed when stream is closed. */
    public var closeBaseStream: Boolean
}
