package io.gitlab.guiVista.io.stream

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.Object
import io.gitlab.guiVista.io.Cancellable
import io.gitlab.guiVista.io.stream.input.InputStreamBase
import io.gitlab.guiVista.io.stream.output.OutputStreamBase

/** A stream source that can be polled. */
public expect class PollableSource {
    public companion object {
        /**
         * Creates a new [PollableSource] that expects a callback of type `GPollableSourceFunc`. The new source
         * doesn't actually do anything on its own; use `g_source_add_child_source()` to add other sources to it to
         * cause it to trigger.
         * @param pollableStream The stream associated with the new source.
         * @return A new instance of [PollableSource].
         */
        public fun create(pollableStream: Object): PollableSource
    }
}

/**
 * Tries to read from [input stream][inputStream], as with [InputStreamBase.read] (if [blocking] is *true*), or
 * [io.gitlab.guiVista.io.stream.input.PollableInputStream.readNonBlocking] (if [blocking] is *false*). This can be
 * used to more easily share code between blocking, and non-blocking implementations of a method. If [blocking] is
 * *false* then [inputStream] must be a [io.gitlab.guiVista.io.stream.input.PollableInputStream] for which
 * [io.gitlab.guiVista.io.stream.input.PollableInputStream.canPoll] is *true*, or else the behavior is undefined. If
 * [blocking] is *true* then [inputStream] doesn't need to be a
 * [io.gitlab.guiVista.io.stream.input.PollableInputStream].
 * @param inputStream The input stream to read from.
 * @param count The number of bytes to read.
 * @param blocking Whether to do blocking IO.
 * @param cancellable The [Cancellable] to use, or *null* to ignore.
 * @param error The [Error] to use, or *null* to ignore.
 * @return A Pair containing the following:
 * 1. bytesRead - The number or bytes read, or *-1* on error.
 * 2. buffer - Contains the data read from [inputStream].
 */
public expect fun readFromStream(
    inputStream: InputStreamBase,
    count: ULong,
    blocking: Boolean,
    cancellable: Cancellable? = null,
    error: Error? = null
): Pair<Long, ByteArray>

/**
 * Tries to write to [output stream][outputStream], as with
 * [io.gitlab.guiVista.io.stream.output.OutputStreamBase.write] (if [blocking] is *true*), or
 * [io.gitlab.guiVista.io.stream.output.PollableOutputStream.writeNonBlocking] (if [blocking] is *false*). This can be
 * used to more easily share code between blocking, and non-blocking implementations of a method. If [blocking] is
 * *false* then [outputStream] **MUST** be a [io.gitlab.guiVista.io.stream.output.PollableOutputStream] for which
 * [io.gitlab.guiVista.io.stream.output.PollableOutputStream.canPoll] is *true* or else the behavior is undefined. If
 * [blocking] is *true* then [outputStream] doesn't need to be a
 * [io.gitlab.guiVista.io.stream.output.PollableOutputStream].
 * @param outputStream The output stream to use.
 * @param buffer The buffer containing the data to write.
 * @param blocking Whether to do blocking IO.
 * @param cancellable The [Cancellable] to use or *null* to ignore.
 * @param error The [Error] to use or *null* to ignore.
 * @return The number of bytes written, or *-1* on error.
 */
public expect fun writeToStream(
    outputStream: OutputStreamBase,
    buffer: ByteArray,
    blocking: Boolean,
    cancellable: Cancellable? = null,
    error: Error? = null
): Long

/**
 * Tries to write count bytes to [output stream][outputStream], as with
 * [io.gitlab.guiVista.io.stream.output.OutputStreamBase.writeAll], but using [writeToStream] rather than
 * [io.gitlab.guiVista.io.stream.output.OutputStreamBase.write]. On a successful write of count bytes, *true* is
 * returned, and bytesWritten is set to count. If there is an error during the operation (including
 * `G_IO_ERROR_WOULD_BLOCK` in the non-blocking case) then *false* is returned, and [error] is set to indicate the
 * error status, bytesWritten is updated to contain the number of bytes written into the stream before the error
 * occurred.
 *
 * As with [writeToStream] if [blocking] is *false* then [outputStream] ***MUST* be a
 * [io.gitlab.guiVista.io.stream.output.PollableOutputStream] for which
 * [io.gitlab.guiVista.io.stream.output.PollableOutputStream.canPoll] is *true* or else the behavior is undefined. If
 * [blocking] is *true* then [outputStream] doesn't need to be a
 * [io.gitlab.guiVista.io.stream.output.PollableOutputStream].
 * @param outputStream The output stream to use.
 * @param buffer The buffer containing the data to write.
 * @param blocking Whether to do blocking IO.
 * @param cancellable The [Cancellable] to use or *null* to ignore.
 * @param error The [Error] to use or *null* to ignore.
 * @return A Pair containing the following:
 * 1. success - Will be *true* if the write operation is successful.
 * 2. bytesWritten - The number of bytes that was written to [outputStream].
 */
public expect fun writeAllToStream(
    outputStream: OutputStreamBase,
    buffer: ByteArray,
    blocking: Boolean,
    cancellable: Cancellable? = null,
    error: Error? = null
): Pair<Boolean, ULong>
