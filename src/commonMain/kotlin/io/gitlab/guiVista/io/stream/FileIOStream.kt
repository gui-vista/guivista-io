package io.gitlab.guiVista.io.stream

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.io.Cancellable
import io.gitlab.guiVista.io.file.FileInfo

/** File read and write streaming operations. */
public expect class FileIOStream : IOStreamBase, Seekable {
    /**
     * Gets the entity tag for the file when it has been written. This **MUST** be called **after** the stream has been
     * written and closed, as the etag can change while writing.
     */
    public val eTag: String

    /**
     * Queries a file io stream for the given [attributes]. This function blocks while querying the stream. For the
     * asynchronous version of this function, see `g_file_io_stream_query_info_async()`. While the stream is blocked
     * the stream will set the pending flag internally, and any other operations on the stream will fail with
     * `G_IO_ERROR_PENDING`. This function can fail if the stream was already closed (with error being set to
     * `G_IO_ERROR_CLOSED`), the stream has pending operations (with [error] being set to `G_IO_ERROR_PENDING`), or if
     * querying info is not supported for the stream's interface (with [error] being set to
     * `G_IO_ERROR_NOT_SUPPORTED`). In all cases of failure, *null* will be returned.
     *
     * If cancellable isn't *null* then the operation can be cancelled by triggering the cancellable object from
     * another thread. If the operation was cancelled then the error `G_IO_ERROR_CANCELLED` will be set, and *null*
     * will be returned.
     * @param attributes A file attribute query [String].
     * @param cancellable The [Cancellable] to use or *null*.
     * @param error The [Error] to use or *null*.
     * @return A instance of [FileInfo], or *null* on error.
     */
    public fun queryInfo(attributes: String, cancellable: Cancellable? = null, error: Error? = null): FileInfo?
}
