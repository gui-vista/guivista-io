package io.gitlab.guiVista.io.stream

import io.gitlab.guiVista.core.Error

/** Stream seeking interface. */
public expect interface Seekable {
    /** Tests if the stream supports [Seekable]. Will be *true* if the [Seekable] can be seeked. */
    public open val canSeek: Boolean

    /** Tests if the length of the stream can be adjusted with [truncate]. */
    public open val canTruncate: Boolean

    /**
     * Tells the current position within the stream.
     * @return The offset from the beginning of the buffer.
     */
    public open fun tell(): Long

    /**
     * Sets the length of the stream to [offset]. If the stream was previously larger than [offset] then the extra
     * data is discarded. If the stream was previously shorter than [offset] then it is extended with null bytes
     * (*'\0'*). The operation can be cancelled by triggering the cancellable object from another thread. If the
     * operation was cancelled then the error `G_IO_ERROR_CANCELLED` will be returned. If an operation was partially
     * finished when the operation was cancelled then the partial result will be returned, without an error.
     * @param offset New length for the [Seekable] in bytes.
     * @param error The [Error] to use for storing the error occurring, or *null* to ignore.
     * @return A value pf *true* if successful. If an error has occurred then this function will return *false*, and
     * set [error] appropriately if present.
     */
    public open fun truncate(offset: Long, error: Error? = null): Boolean
}
