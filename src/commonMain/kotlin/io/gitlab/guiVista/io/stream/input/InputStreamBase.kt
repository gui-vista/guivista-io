package io.gitlab.guiVista.io.stream.input

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.io.Cancellable

/** Base interface for implementing streaming input. */
public expect interface InputStreamBase : ObjectBase {
    /** Checks if an input stream is closed. */
    public open val streamIsClosed: Boolean

    /** Checks if an input stream has pending actions. */
    public open val hasPending: Boolean

    /**
     * Sets stream to have actions pending. If the pending flag is already set or stream is closed, it will return
     * *false* and set [error].
     * @param error The [Error] to use or *null*.
     * @return A value of *true* if pending was previously unset and is now set.
     */
    public open fun changePending(error: Error? = null): Boolean

    /**
     * Clears the pending flag on the stream.
     */
    public open fun clearPending()

    /**
     * Tries to read count bytes from the stream into the buffer starting at the buffer. Will block during this read.
     * If [count] is zero then this function returns zero and does nothing. A value of [count] larger than `G_MAXSSIZE`
     * will cause a `G_IO_ERROR_INVALID_ARGUMENT` error. On success the number of bytes read into the buffer is
     * returned. It is not an error if this is not the same as the requested size, as it can happen e.g. near the end
     * of a file. Zero is returned on end of file (or if [count] is zero), but never otherwise.
     *
     * The returned buffer is not a null terminated string. It can contain null bytes at any position, and this
     * function doesn't null terminate the buffer. If cancellable isn't *null* then the operation can be cancelled by
     * triggering the cancellable object from another thread. If the operation was cancelled then the error
     * `G_IO_ERROR_CANCELLED` will be returned. If an operation was partially finished when the operation was cancelled
     * the partial result will be returned, without an error. On error *-1* is returned, and error is set accordingly.
     * @param count The number of bytes that will be read from the stream.
     * @param cancellable An optional [Cancellable], or *null* to ignore.
     * @param error The error to use or *null* to ignore.
     * @return A Pair containing the following:
     * 1. buffer - Contains data read from the input stream.
     * 2. bytesRead - Number of bytes read, *-1* on error, or *0* on end of file.
     */
    public open fun read(count: ULong, cancellable: Cancellable? = null, error: Error? = null): Pair<ByteArray, Long>

    /**
     * Tries to read [count] bytes from the stream into the buffer starting at the buffer. Will block during this
     * read. This function is similar to [read], except it tries to read as many bytes as requested, only stopping on
     * an error or end of stream. On a successful read of count bytes, or if we reached the end of the stream, *true*
     * is returned, and bytesRead is set to the number of bytes read into the buffer.
     *
     * If there is an error during the operation *false* is returned, and [error] is set to indicate the error status.
     * As a special exception to the normal conventions for functions that use [Error], if this function returns
     * *false* (and sets [error]) then bytesRead will be set to the number of bytes that were successfully read
     * **before** the error was encountered.
     * @param count The number of bytes that will be read from the stream.
     * @param cancellable The [Cancellable] to use or *null*.
     * @param error The [Error] to use or *null*.
     * @return A Triple containing the following:
     * 1. buffer - Contains data read from the input stream.
     * 2. bytesRead - The number of bytes read from the input stream.
     * 3. success - Will be *true* is no error was encountered.
     */
    public open fun readAll(
        count: ULong,
        cancellable: Cancellable? = null,
        error: Error? = null
    ): Triple<ByteArray, ULong, Boolean>

    /**
     * Tries to skip [count] bytes from the stream. Will block during the operation. This is identical to [read], from
     * a behaviour standpoint, but the bytes that are skipped are not returned to the user. Some streams have an
     * implementation that is more efficient than reading the data. his function is optional for inherited classes, as
     * the default implementation emulates it using read.
     *
     * If [cancellable] isn't *null* then the operation can be cancelled by triggering the cancellable object from
     * another thread. If the operation was cancelled then the error `G_IO_ERROR_CANCELLED` will be returned. If an
     * operation was partially finished when the operation was cancelled the partial result will be returned, without
     * an error.
     * @param count The number of bytes that will be skipped from the stream.
     * @param cancellable The [Cancellable] to use or *null*.
     * @param error The [Error] to use or *null*.
     * @return Number of bytes skipped, or *-1* on error.
     */
    public open fun skip(count: ULong, cancellable: Cancellable? = null, error: Error? = null): Long

    /**
     * Closes the stream, releasing resources related to it. Once the stream is closed, all other operations will
     * return `G_IO_ERROR_CLOSED`. Closing a stream multiple times will not return an error. Streams will be
     * automatically closed when the last reference is dropped, but you might want to call this function to make sure
     * resources are released as early as possible.
     *
     * Some streams might keep the backing store of the stream (e.g. a file descriptor) open after the stream is
     * closed. See the documentation for the individual stream for details. On failure the first error that happened
     * will be reported, but the close operation will finish as much as possible. A stream that failed to close will
     * still return `G_IO_ERROR_CLOSED` for all operations. Still it is important to check, and report the error to
     * the user.
     *
     * If cancellable isn't *null* then the operation can be cancelled by triggering the cancellable object from
     * another thread. If the operation was cancelled then the error `G_IO_ERROR_CANCELLED` will be returned.
     * Cancelling a close will still leave the stream closed, but some streams can use a faster close that doesn't
     * block to e.g. check errors.
     * @param cancellable The [Cancellable] to use or *null*.
     * @param error The [Error] to use or *null*.
     * @return A value of *true* on success, *false* on failure.
     */
    public open fun closeStream(cancellable: Cancellable? = null, error: Error? = null): Boolean
}
