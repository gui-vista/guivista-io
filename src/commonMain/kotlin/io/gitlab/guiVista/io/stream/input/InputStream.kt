package io.gitlab.guiVista.io.stream.input

/** Default implementation of [InputStreamBase]. */
public expect class InputStream : InputStreamBase
