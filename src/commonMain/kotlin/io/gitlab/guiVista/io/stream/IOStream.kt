package io.gitlab.guiVista.io.stream

/** Default implementation of [IOStreamBase]. */
public expect class IOStream : IOStreamBase
