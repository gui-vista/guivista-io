package io.gitlab.guiVista.io.stream.output

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.io.Cancellable
import io.gitlab.guiVista.io.stream.Seekable

/** Includes functions for writing data directly to an output stream. */
public expect class DataOutputStream : FilterOutputStream, Seekable {
    public companion object {
        public fun create(baseStream: OutputStreamBase): DataOutputStream
    }

    /**
     * Puts a byte into the output stream.
     * @param data The data to add to the stream.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return A value of *true* if [data] was successfully added to the stream.
     */
    public fun putByte(data: Byte, cancellable: Cancellable? = null, error: Error? = null): Boolean

    /**
     * Puts a signed integer into the output stream.
     * @param data The data to add to the stream.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return A value of *true* if [data] was successfully added to the stream.
     */
    public fun putInt(data: Int, cancellable: Cancellable? = null, error: Error? = null): Boolean

    /**
     * Puts a unsigned integer into the output stream.
     * @param data The data to add to the stream.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return A value of *true* if [data] was successfully added to the stream.
     */
    public fun putUInt(data: UInt, cancellable: Cancellable? = null, error: Error? = null): Boolean

    /**
     * Puts a signed long into the output stream.
     * @param data The data to add to the stream.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return A value of *true* if [data] was successfully added to the stream.
     */
    public fun putLong(data: Long, cancellable: Cancellable? = null, error: Error? = null): Boolean

    /**
     * Puts a unsigned long into the output stream.
     * @param data The data to add to the stream.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return A value of *true* if [data] was successfully added to the stream.
     */
    public fun putULong(data: ULong, cancellable: Cancellable? = null, error: Error? = null): Boolean

    /**
     * Puts a signed short into the output stream.
     * @param data The data to add to the stream.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return A value of *true* if [data] was successfully added to the stream.
     */
    public fun putShort(data: Short, cancellable: Cancellable? = null, error: Error? = null): Boolean

    /**
     * Puts a unsigned short into the output stream.
     * @param data The data to add to the stream.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return A value of *true* if [data] was successfully added to the stream.
     */
    public fun putUShort(data: UShort, cancellable: Cancellable? = null, error: Error? = null): Boolean

    /**
     * Puts a string into the output stream.
     * @param data The data to add to the stream.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return A value of *true* if [data] was successfully added to the stream.
     */
    public fun putString(data: String, cancellable: Cancellable? = null, error: Error? = null): Boolean
}
