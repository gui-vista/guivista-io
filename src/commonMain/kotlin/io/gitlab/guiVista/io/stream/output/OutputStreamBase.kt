package io.gitlab.guiVista.io.stream.output

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.io.Cancellable

/** Base interface for implementing streaming output. */
public expect interface OutputStreamBase : ObjectBase {
    /**
     * Checks if an output stream is being closed. This can be used inside e.g. a flush implementation to see if the
     * flush (or other i/o operation) is called from within the closing operation.
     */
    public open val streamIsClosing: Boolean

    /** Checks if an output stream has already been closed. */
    public open val streamIsClosed: Boolean

    /** Checks if an output stream has pending actions. */
    public open val hasPending: Boolean

    /**
     * Sets stream to have actions pending. If the pending flag is already set or stream is closed, it will return
     * *false* and set [error].
     * @param error The [Error] to use or *null* to ignore.
     * @return A value of *true* if pending was previously unset and is now set.
     */
    public open fun changePending(error: Error? = null): Boolean

    /** Clears the pending flag on stream. */
    public open fun clearPending()

    /**
     * Tries to write bytes from [buffer] into the stream. Will block during the operation. If count of [buffer] is
     * *0*, then return *0* and do nothing. A count larger than `G_MAXSSIZE` will cause a
     * `G_IO_ERROR_INVALID_ARGUMENT` error. On success the number of bytes written to the stream is returned. It is
     * not an error if this is not the same as the requested size, as it can happen e.g. on a partial I/O error, or
     * if there is not enough storage in the stream. All writes block until at least one byte is written or an error
     * occurs; *0* is never returned (unless the count is 0).
     *
     * If [cancellable] isn't *null* then the operation can be cancelled by triggering the cancellable object from
     * another thread. If the operation was cancelled then the error `G_IO_ERROR_CANCELLED` will be returned. If an
     * operation was partially finished when the operation was cancelled the partial result will be returned, without
     * an error. On error *-1* is returned, and [error] is set accordingly.
     * @param buffer The buffer containing the data to write.
     * @param cancellable The [Cancellable] to use or *null*.
     * @param error The [Error] to use or *null*.
     * @return Number of bytes written, or *-1* on error.
     */
    public open fun write(buffer: ByteArray, cancellable: Cancellable? = null, error: Error? = null): Long

    /**
     * Tries to write bytes from [buffer] into the stream. Will block during the operation. This function is similar to
     * [write], except it tries to write as many bytes as requested, only stopping on an error. On a successful write
     * of count bytes, *true* is returned, and bytesWritten is set to the size of the buffer.
     *
     * If there is an error during the operation then *false* is returned, and [error] is set to indicate the error
     * status. As a special exception to the normal conventions for functions that use [Error], if this function
     * returns *false* (and sets [error]) then bytesWritten will be set to the number of bytes that were successfully
     * written **before** the error was encountered.
     * @param buffer The buffer containing the data to write.
     * @param cancellable The [Cancellable] to use or *null*.
     * @param error The [Error] to use or *null*.
     * @return A Pair containing the following:
     * 1. bytesWritten - The number of bytes that was written to the stream.
     * 2. success - Will be *true* on success, *false* if there was an error.
     */
    public open fun writeAll(
        buffer: ByteArray,
        cancellable: Cancellable? = null,
        error: Error? = null
    ): Pair<ULong, Boolean>

    /**
     * Forces a write of all user-space buffered data for the given stream. Will block during the operation. Closing
     * the stream will implicitly cause a flush. This function is optional for inherited classes. If [cancellable]
     * isn't *null* then the operation can be cancelled by triggering the cancellable object from another thread. If
     * the operation was cancelled then the error `G_IO_ERROR_CANCELLED` will be returned.
     * @param cancellable The [Cancellable] to use or *null*.
     * @param error The [Error] to use or *null* to ignore.
     * @return A value of *true on success*, *false* on error.
     */
    public open fun flush(cancellable: Cancellable? = null, error: Error? = null)

    /**
     * Closes the stream, releasing resources related to it. Once the stream is closed, all other operations will
     * return `G_IO_ERROR_CLOSED`. Closing a stream multiple times will not return an error. Closing a stream will
     * automatically flush any outstanding buffers in the stream. Streams will be automatically closed when the last
     * reference is dropped, but you might want to call this function to make sure resources are released as early as
     * possible.
     *
     * Some streams might keep the backing store of the stream (e.g. a file descriptor) open after the stream is
     * closed. See the documentation for the individual stream for details. On failure the first error that happened
     * will be reported, but the close operation will finish as much as possible. A stream that failed to close will
     * still return `G_IO_ERROR_CLOSED` for all operations. Still, it is important to check and report the error to
     * the user, otherwise there might be a loss of data as all data might not be written.
     *
     * If [cancellable] isn't *null* then the operation can be cancelled by triggering the cancellable object from
     * another thread. If the operation was cancelled then the error `G_IO_ERROR_CANCELLED` will be returned.
     * Cancelling a close will still leave the stream closed, but there some streams can use a faster close that
     * doesn't block to e.g. check errors. On cancellation (as with any error) there is no guarantee that all written
     * data will reach the target.
     */
    public open fun closeStream(cancellable: Cancellable? = null, error: Error? = null): Boolean
}
