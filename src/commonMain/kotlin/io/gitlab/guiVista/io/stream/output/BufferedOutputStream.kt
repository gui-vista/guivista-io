package io.gitlab.guiVista.io.stream.output

/** Default implementation of [BufferedOutputStreamBase]. */
public expect class BufferedOutputStream : BufferedOutputStreamBase {
    public companion object {
        /**
         * Creates a new [BufferedOutputStream] from the given [baseStream], with a buffer set to [size], or the buffer
         * set to a default size (4 KB) if [size] is *0*.
         * @param baseStream The input stream to use.
         * @param size The size of the buffer.
         * @return A new [BufferedOutputStream] instance for the given [baseStream].
         */
        public fun create(baseStream: OutputStreamBase, size: ULong = 0uL): BufferedOutputStream
    }
}
