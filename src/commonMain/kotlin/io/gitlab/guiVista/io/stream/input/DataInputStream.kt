package io.gitlab.guiVista.io.stream.input

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.io.Cancellable
import io.gitlab.guiVista.io.stream.Seekable

/** Includes functions for reading structured data directly from a binary input stream. */
public expect class DataInputStream : BufferedInputStreamBase, Seekable {
    public companion object {
        public fun create(baseStream: InputStreamBase): DataInputStream
    }

    /**
     * Reads an unsigned byte value from the stream.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return An unsigned byte value read from the stream, or *0* if an error occurred.
     */
    public fun readUByte(cancellable: Cancellable?, error: Error?): UByte

    /**
     * Reads a signed short value from the stream.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return A signed short value read from the stream, or *0* if an error occurred.
     */
    public fun readShort(cancellable: Cancellable?, error: Error?): Short

    /**
     * Reads a unsigned short value from the stream.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return An unsigned short value read from the stream, or *0* if an error occurred.
     */
    public fun readUShort(cancellable: Cancellable?, error: Error?): UShort

    /**
     * Reads a signed integer value from the stream.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return A signed integer value read from the stream, or *0* if an error occurred.
     */
    public fun readInt(cancellable: Cancellable?, error: Error?): Int

    /**
     * Reads a unsigned integer value from the stream.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return An unsigned integer value read from the stream, or *0* if an error occurred.
     */
    public fun readUInt(cancellable: Cancellable?, error: Error?): UInt

    /**
     * Reads a signed long value from the stream.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return A signed long value read from the stream, or *0* if an error occurred.
     */
    public fun readLong(cancellable: Cancellable?, error: Error?): Long

    /**
     * Reads a unsigned long value from the stream.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return An unsigned long value read from the stream, or *0* if an error occurred.
     */
    public fun readULong(cancellable: Cancellable?, error: Error?): ULong

    /**
     * Reads a line from the data input stream. Note that no encoding checks or conversion is performed; the input is
     * not guaranteed to be UTF-8, and may in fact have embedded null characters. If [cancellable] isn't *null* then
     * the operation can be cancelled by triggering the cancellable object from another thread. If the operation was
     * cancelled then the error `G_IO_ERROR_CANCELLED` will be returned.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return A Pair containing the following:
     * 1. length - The size of the data that has been read.
     * 2. data - An array of data that has been read from the data input stream. On an error data will be *null*, and
     * [error] will be set. If there's no content to read then data will be *null*, but [error] won't be set.
     */
    public fun readLine(cancellable: Cancellable? = null, error: Error? = null): Pair<ULong, String?>

    /**
     * Reads a UTF-8 encoded line from the data input stream. If [cancellable] isn't *null* then the operation can be
     * cancelled by triggering the cancellable object from another thread. If the operation was cancelled then the
     * error `G_IO_ERROR_CANCELLED` will be returned.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return A Pair containing the following:
     * 1. length - The size of the data that has been read.
     * 2. data - The data that has been read from the data input stream as a [String]. On an error data will be
     * *null* and [error] will be set. For UTF-8 conversion errors the set error domain is `G_CONVERT_ERROR`. If
     * there's no content to read then data will be *null*, but [error] won't be set.
     */
    public fun readUtf8Line(cancellable: Cancellable? = null, error: Error? = null): Pair<ULong, String?>

    /**
     * Reads a string from the data input stream, up to the first occurrence of any of the stop characters.
     * @return A Pair containing the following:
     * 1. length - The size of the data that has been read.
     * 2. data - The data that has been read from the data input stream as a [String] before encountering any of the
     * stop characters. If an error is encountered then data is *null*.
     */
    public fun readUpto(stopChars: String, cancellable: Cancellable? = null, error: Error? = null): Pair<ULong, String?>
}
