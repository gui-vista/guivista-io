package io.gitlab.guiVista.io.stream.input

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.io.Cancellable

/** Interface for pollable input streams. */
public expect interface PollableInputStream {
    /**
     * Checks if stream is actually pollable. Some classes may implement [PollableInputStream] but have only certain
     * instances of that class be pollable. If this property is *false* then the behavior of other
     * [PollableInputStream] functions/properties is undefined. For any given stream the value returned by this
     * property is constant; a stream cannot switch from pollable to non-pollable or vice versa.
     */
    public open val canPoll: Boolean

    /**
     * Checks if stream can be read. Note that some stream types may not be able to implement this 100% reliably, and
     * it is possible that a call to [io.gitlab.guiVista.io.stream.input.InputStreamBase.read] after this returns
     * *true* would still block. To guarantee non-blocking behavior, you should always use
     * [readNonBlocking], which will return a `G_IO_ERROR_WOULD_BLOCK` error rather than blocking.
     */
    public open val isReadable: Boolean

    /**
     * Attempts to read up to [count] bytes from the stream into the buffer, as with
     * [io.gitlab.guiVista.io.stream.input.InputStreamBase.read]. If the stream is not currently readable then this
     * function will immediately return `G_IO_ERROR_WOULD_BLOCK`, and you can use
     * `g_pollable_input_stream_create_source()` to create a `GSource` that will be triggered when stream is readable.
     *
     * Note that since this function never blocks, you cannot actually use [cancellable] to cancel it. However it this
     * function will return an error if [cancellable] has already been cancelled when you call, which may happen if
     * you call this function after a source triggers due to having been cancelled.
     * @param count The number of bytes you want to read.
     * @param cancellable The [Cancellable] to use or *null*.
     * @param error The [Error] to use or *null*.
     * @return A Pair containing the following:
     * 1. bytesRead - The number of bytes read or *-1* on error (including `G_IO_ERROR_WOULD_BLOCK`).
     * 2. buffer - The buffer that holds the read data.
     */
    public open fun readNonBlocking(
        count: ULong,
        cancellable: Cancellable? = null,
        error: Error? = null
    ): Pair<Long, ByteArray>
}
