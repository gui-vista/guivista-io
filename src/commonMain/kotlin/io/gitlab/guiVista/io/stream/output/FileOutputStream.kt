package io.gitlab.guiVista.io.stream.output

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.io.Cancellable
import io.gitlab.guiVista.io.file.FileInfo

/** File output streaming operations. */
public expect class FileOutputStream : OutputStreamBase {
    /**
     * Gets the entity tag for the file when it has been written. This property **MUST** be accessed **after** the
     * stream has been written and closed, as the eTag can change while writing.
     */
    public val eTag: String

    /**
     * Queries a file output stream for the given [attributes]. This function blocks while querying the stream. For
     * the asynchronous version of this function, see `g_file_output_stream_query_info_async()`. While the stream is
     * blocked, the stream will set the pending flag internally, and any other operations on the stream will fail with
     * `G_IO_ERROR_PENDING`. Can fail if the stream was already closed (with [error] being set to `G_IO_ERROR_CLOSED`),
     * the stream has pending operations (with [error] being set to `G_IO_ERROR_PENDING`), or if querying info is not
     * supported for the stream's interface (with [error] being set to `G_IO_ERROR_NOT_SUPPORTED`). In all cases of
     * failure, *null* will be returned.
     *
     * If cancellable isn't *null* then the operation can be cancelled by triggering the cancellable object from
     * another thread. If the operation was cancelled then the error `G_IO_ERROR_CANCELLED` will be set, and *null*
     * will be returned.
     * @param attributes A file attribute query [String].
     * @param cancellable The [Cancellable] to use or *null*.
     * @param error The [Error] to use or *null*.
     * @return A instance of [FileInfo], or *null* on error.
     */
    public fun queryInfo(attributes: String, cancellable: Cancellable? = null, error: Error? = null): FileInfo?
}
