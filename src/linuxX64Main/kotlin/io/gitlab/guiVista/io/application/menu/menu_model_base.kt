package io.gitlab.guiVista.io.application.menu

import gio2.*
import glib2.TRUE
import glib2.gpointer
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.dataType.variant.Variant
import io.gitlab.guiVista.core.dataType.variant.VariantType
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer

public actual interface MenuModelBase : ObjectBase {
    public val gMenuModelPtr: CPointer<GMenuModel>?

    public actual val isMutable: Boolean
        get() = g_menu_model_is_mutable(gMenuModelPtr) == TRUE

    public actual val totalItems: Int
        get() = g_menu_model_get_n_items(gMenuModelPtr)

    public actual fun fetchItemAttributeValue(itemIndex: Int, attrib: String, expectedType: VariantType?): Variant? {
        val ptr = g_menu_model_get_item_attribute_value(
            model = gMenuModelPtr,
            item_index = itemIndex,
            attribute = attrib,
            expected_type = expectedType?.gVariantTypePtr
        )
        return if (ptr != null) Variant.fromPointer(ptr) else null
    }

    public actual fun fetchItemAttribute(itemIndex: Int, attrib: String, formatStr: String): Boolean =
        g_menu_model_get_item_attribute(
            model = gMenuModelPtr,
            item_index = itemIndex,
            attribute = attrib,
            format_string = formatStr
        ) == TRUE

    public actual fun fetchItemLink(itemIndex: Int, link: String): MenuModelBase? {
        val ptr = g_menu_model_get_item_link(model = gMenuModelPtr, item_index = itemIndex, link = link)
        return if (ptr != null) MenuModel(ptr) else null
    }

    public actual fun iterateItemAttributes(itemIndex: Int): MenuAttributeIterator =
        MenuAttributeIterator(g_menu_model_iterate_item_attributes(gMenuModelPtr, itemIndex))

    public actual fun iterateItemLinks(itemIndex: Int): MenuLinkIterator =
        MenuLinkIterator(g_menu_model_iterate_item_links(gMenuModelPtr, itemIndex))

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gMenuModelPtr, handlerId)
    }

    /**
     * Connects the *items-changed* event to a [handler] on a menu model. This event occurs when a change has occurred
     * to the menu. The only changes that can occur to a menu is that items are removed or added. Items may not change
     * (except by being removed and added back in the same location). This event is capable of describing both of
     * those changes (at the same time).
     *
     * The event means that starting at the index position, removed items were removed and added items were added in
     * their place. If removed is zero then only items were added. If added is zero then only items were removed. As an
     * example if the menu contains items a, b, c, d (in that order), and the event (2, 1, 3) occurs then the new
     * composition of the menu will be a, b, _, _, _, d (with each _ representing some new item).
     *
     * Event handlers may query the model (particularly the added items), and expect to see the results of the
     * modification that is being reported. The event is emitted after the modification.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     * @return The handler ID.
     */
    public fun connectItemsChangedEvent(handler: CPointer<ItemsChangedHandler>,
                                        userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gMenuModelPtr, signal = MenuModelBaseEvent.itemsChanged, slot = handler,
            data = userData)
}

/**
 * The event handler for the *items-changed* event. Arguments:
 * 1. model: CPointer<GMenuModel>
 * 2. pos: Int
 * 3. removed: Int
 * 4. added: Int
 * 5. userData: gpointer
 */
public typealias ItemsChangedHandler = CFunction<(
    model: CPointer<GMenuModel>,
    pos: Int,
    removed: Int,
    added: Int,
    userData: gpointer
) -> Unit>
