package io.gitlab.guiVista.io.application.action

import gio2.*
import glib2.*
import kotlinx.cinterop.*
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.dataType.variant.Variant
import io.gitlab.guiVista.core.dataType.variant.VariantType
import io.gitlab.guiVista.core.disconnectGSignal

public actual interface ActionGroupBase : ObjectBase {
    public val gActionGroupPtr: CPointer<GActionGroup>?

    public actual fun listActions(): Array<String> {
        val tmpResult = mutableListOf<String>()
        val tmpActions = g_action_group_list_actions(gActionGroupPtr)
        var running = true
        var pos = 0
        while (running) {
            val item = tmpActions?.get(pos)
            if (item == null) running = false
            else tmpResult += item.toKString()
            pos++
        }
        g_strfreev(tmpActions)
        return tmpResult.toTypedArray()
    }

    public actual fun queryAction(
        actionName: String,
        enabled: Boolean,
        paramType: VariantType?,
        stateType: VariantType?,
        stateHint: Variant,
        state: Variant
    ): Boolean = g_action_group_query_action(
        action_group = gActionGroupPtr,
        action_name = actionName,
        parameter_type = cValuesOf(paramType?.gVariantTypePtr),
        state_type = cValuesOf(stateType?.gVariantTypePtr),
        state_hint = cValuesOf(stateHint.gVariantPtr),
        state = cValuesOf(state.gVariantPtr),
        enabled = if (enabled) cValuesOf(TRUE) else cValuesOf(FALSE)
    ) == TRUE

    public actual fun hasAction(actionName: String): Boolean =
        g_action_group_has_action(gActionGroupPtr, actionName) == TRUE

    public actual fun fetchActionEnabled(actionName: String): Boolean =
        g_action_group_get_action_enabled(gActionGroupPtr, actionName) == TRUE

    public actual fun fetchActionParameterType(actionName: String): VariantType? {
        val ptr = g_action_group_get_action_parameter_type(gActionGroupPtr, actionName)
        return if (ptr != null) VariantType.fromPointer(ptr) else null
    }

    public actual fun fetchActionStateType(actionName: String): VariantType? {
        val ptr = g_action_group_get_action_state_type(gActionGroupPtr, actionName)
        return if (ptr != null) VariantType.fromPointer(ptr) else null
    }

    public actual fun fetchActionStateHint(actionName: String): Variant? {
        val ptr = g_action_group_get_action_state_hint(gActionGroupPtr, actionName)
        return if (ptr != null) Variant.fromPointer(ptr) else null
    }

    public actual fun fetchActionState(actionName: String): Variant? {
        val ptr = g_action_group_get_action_state(gActionGroupPtr, actionName)
        return if (ptr != null) Variant.fromPointer(ptr) else null
    }

    public actual fun changeActionState(actionName: String, value: Variant) {
        g_action_group_change_action_state(action_group = gActionGroupPtr, action_name = actionName,
            value = value.gVariantPtr)
    }

    public actual fun activateAction(actionName: String, param: Variant?) {
        g_action_group_activate_action(action_group = gActionGroupPtr, action_name = actionName,
            parameter = param?.gVariantPtr)
    }

    /**
     * Connects the *action-added* event to a [handler] on a [ActionGroupBase]. This event occurs after the action has
     * been added and is now visible.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectActionAddedEvent(handler: CPointer<ActionAddedHandler>, userData: gpointer): ULong =
        connectGSignal(obj = gActionGroupPtr, signal = ActionGroupBaseEvent.actionAdded, slot = handler,
            data = userData)

    /**
     * Connects the *action-enabled-changed* event to a [handler] on a [ActionGroupBase]. This event indicates that the
     * enabled status of the named action has changed.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectActionEnabledChangedEvent(
        handler: CPointer<ActionEnabledChangedHandler>,
        userData: gpointer
    ): ULong =
        connectGSignal(obj = gActionGroupPtr, signal = ActionGroupBaseEvent.actionEnabledChanged, slot = handler,
            data = userData)

    /**
     * Connects the *action-removed* event to a [handler] on a [ActionGroupBase]. This event occurs before the action
     * is removed, so the action is still visible, and can be queried from the event handler.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectActionRemovedEvent(handler: CPointer<ActionRemovedHandler>, userData: gpointer): ULong =
        connectGSignal(obj = gActionGroupPtr, signal = ActionGroupBaseEvent.actionRemoved, slot = handler,
            data = userData)

    /**
     * Connects the *action-state-changed* event to a [handler] on a [ActionGroupBase]. This event indicates that the
     * state of the named action has changed.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectActionStateChangedEvent(
        handler: CPointer<ActionStateChangedHandler>,
        userData: gpointer
    ): ULong =
        connectGSignal(obj = gActionGroupPtr, signal = ActionGroupBaseEvent.actionStateChanged, slot = handler,
            data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gActionGroupPtr, handlerId)
    }
}

/**
 * The event handler for the *action-added* event. Arguments:
 * 1. actionGroup: CPointer<GActionGroup>
 * 2. actionName: CPointer<ByteVar>
 * 3. userData: gpointer
 */
public typealias ActionAddedHandler = CFunction<(
    actionGroup: CPointer<GActionGroup>,
    actionName: CPointer<ByteVar>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *action-enabled-changed* event. Arguments:
 * 1. actionGroup: CPointer<GActionGroup>
 * 2. actionName: CPointer<ByteVar>
 * 3. enabled: Int
 * 4. userData: gpointer
 */
public typealias ActionEnabledChangedHandler = CFunction<(
    actionGroup: CPointer<GActionGroup>,
    actionName: CPointer<ByteVar>,
    enabled: Int,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *action-removed* event. Arguments:
 * 1. actionGroup: CPointer<GActionGroup>
 * 2. actionName: CPointer<ByteVar>
 * 3. userData: gpointer
 */
public typealias ActionRemovedHandler = CFunction<(
    actionGroup: CPointer<GActionGroup>,
    actionName: CPointer<ByteVar>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *action-state-changed* event. Arguments:
 * 1. actionGroup: CPointer<GActionGroup>
 * 2. actionName: CPointer<ByteVar>
 * 3. value: CPointer<GVariant>
 * 4. userData: gpointer
 */
public typealias ActionStateChangedHandler = CFunction<(
    actionGroup: CPointer<GActionGroup>,
    actionName: CPointer<ByteVar>,
    value: CPointer<GVariant>,
    userData: gpointer
) -> Unit>
