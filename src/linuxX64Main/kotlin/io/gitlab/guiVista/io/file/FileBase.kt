package io.gitlab.guiVista.io.file

import gio2.*
import glib2.FALSE
import glib2.TRUE
import glib2.g_free
import io.gitlab.guiVista.core.Closable
import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.io.Cancellable
import io.gitlab.guiVista.io.stream.FileIOStream
import io.gitlab.guiVista.io.stream.input.FileInputStream
import io.gitlab.guiVista.io.stream.output.FileOutputStream
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.cValuesOf
import kotlinx.cinterop.toKString

public actual interface FileBase : Closable {
    public val gFilePtr: CPointer<GFile>?
    public actual val baseName: String
        get() = g_file_get_basename(gFilePtr)?.toKString() ?: ""
    public actual val path: String
        get() = g_file_get_path(gFilePtr)?.toKString() ?: ""
    public actual val uri: String
        get() = g_file_get_uri(gFilePtr)?.toKString() ?: ""
    public actual val parent: FileBase?
        get() {
            val ptr = g_file_get_parent(gFilePtr)
            return if (ptr != null) File.fromPointer(ptr) else null
        }
    public actual val peekPath: String
        get() = g_file_peek_path(gFilePtr)?.toKString() ?: ""
    public actual val isNative: Boolean
        get() = g_file_is_native(gFilePtr) == TRUE
    public actual val uriScheme: String
        get() {
            val ptr = g_file_get_uri_scheme(gFilePtr)
            val result = ptr?.toKString() ?: ""
            g_free(ptr)
            return result
        }

    public actual fun hasUriScheme(uriScheme: String): Boolean = g_file_has_uri_scheme(gFilePtr, uri) == TRUE

    public actual fun hasParent(parent: FileBase): Boolean = g_file_has_parent(gFilePtr, parent.gFilePtr) == TRUE

    public actual fun fetchChild(name: String): FileBase = File.fromPointer(g_file_get_child(gFilePtr, name))

    public actual fun hasPrefix(prefix: FileBase): Boolean = g_file_has_prefix(gFilePtr, prefix.gFilePtr) == TRUE

    public actual fun fetchParseName(): String = g_file_get_parse_name(gFilePtr)?.toKString() ?: ""

    public actual fun fetchRelativePath(descendant: FileBase): String =
        g_file_get_relative_path(gFilePtr, descendant.gFilePtr)?.toKString() ?: ""

    public actual fun resolveRelativePath(relativePath: String): FileBase? {
        val ptr = g_file_resolve_relative_path(gFilePtr, relativePath)
        return if (ptr != null) File.fromPointer(ptr) else null
    }

    /**
     * Gets the requested information about the files in a directory. The result is a [FileEnumerator] object that will
     * give out GFileInfo objects for all the files in the directory. The [attributes] value is a string that specifies
     * the file attributes that should be gathered. It is not an error if it's not possible to read a particular
     * requested attribute from a file - it just won't be set. Attributes should be a comma-separated list of
     * attributes, or attribute wildcards. The wildcard "*" means all attributes, and a wildcard like "standard::*"
     * means all attributes in the standard namespace. An example attribute query be "standard::*,owner::user". The
     * standard attributes are available as defines, like *G_FILE_ATTRIBUTE_STANDARD_NAME*.
     *
     * If cancellable isn't *null* then the operation can be cancelled by triggering the cancellable object from
     * another thread. If the operation was cancelled the error *G_IO_ERROR_CANCELLED* will be returned. If the file
     * doesn't exist the *G_IO_ERROR_NOT_FOUND* error will be returned. If the file isn't a directory the
     * *G_IO_ERROR_NOT_DIRECTORY* error will be returned. Other errors are possible too.
     * @param attributes An attribute query string.
     * @param flags A set of GFileQueryInfoFlags.
     * @return A [FileEnumerator] if successful, *null* on error.
     */
    public fun enumerateChildren(attributes: String, flags: GAppInfoCreateFlags): FileEnumerator? {
        val tmp = g_file_enumerate_children(
            file = gFilePtr,
            attributes = attributes,
            flags = flags,
            cancellable = null,
            error = null
        )
        return if (tmp != null) FileEnumerator(tmp) else null
    }

    /**
     * Obtains a directory monitor for the given file. This may fail if directory monitoring isn't supported. If the
     * operation was cancelled then the error `G_IO_ERROR_CANCELLED` will be returned. It does not make sense for
     * [flags] to contain `G_FILE_MONITOR_WATCH_HARD_LINKS`, since hard links can not be made to directories. It is
     * not possible to monitor all the files in a directory for changes made via hard links. If you want to do this
     * then you **MUST** register individual watches with [monitor].
     * @param flags A set of flags to use.
     * @param error The [Error] to use or *null*.
     * @return A [FileMonitor] for the given file, or *null* on error. Use the **close** function on the returned
     * object when you are finished with it.
     */
    public fun monitorDirectory(flags: GFileMonitorFlags, error: Error? = null): FileMonitor? {
        val ptr = g_file_monitor_directory(file = gFilePtr, flags = flags, error = cValuesOf(error?.gErrorPtr),
            cancellable = null)
        return if (ptr != null) FileMonitor.fromPointer(ptr) else null
    }

    /**
     * Obtains a file monitor for the given file. If no file notification mechanism exists then regular polling of the
     * file is used. If the operation was cancelled then the error `G_IO_ERROR_CANCELLED` will be returned. If [flags]
     * contains `G_FILE_MONITOR_WATCH_HARD_LINKS` then the monitor will also attempt to report changes made to the
     * file via another filename (ie, a hard link). Without this flag you can only rely on changes made through the
     * filename contained in file to be reported. Using this flag may result in an increase in resource usage, and may
     * not have any effect depending on the [FileMonitor] backend and/or filesystem type.
     * @param flags A set of flags to use.
     * @param error The [Error] to use or *null*.
     * @return A [FileMonitor] for the given file, or *null* on error. Use the **close** function on the returned
     * object when you are finished with it.
     */
    public fun monitorFile(flags: GFileMonitorFlags, error: Error? = null): FileMonitor? {
        val ptr = g_file_monitor_file(file = gFilePtr, flags = flags, error = cValuesOf(error?.gErrorPtr),
            cancellable = null)
        return if (ptr != null) FileMonitor.fromPointer(ptr) else null
    }

    /**
     * Obtains a file or directory monitor for the given file, depending on the type of the file. If the operation was
     * cancelled then the error `G_IO_ERROR_CANCELLED` will be returned.
     * @param flags A set of flags to use.
     * @param error The [Error] to use or *null*.
     * @return A [FileMonitor] for the given file, or *null* on error. Use the **close** function on the returned
     * object when you are finished with it.
     */
    public fun monitor(flags: GFileMonitorFlags, error: Error? = null): FileMonitor? {
        val ptr = g_file_monitor(file = gFilePtr, flags = flags, error = cValuesOf(error?.gErrorPtr),
            cancellable = null)
        return if (ptr != null) FileMonitor.fromPointer(ptr) else null
    }

    /**
     * Utility function to inspect the GFileType of a [File]. This function does blocking I/O. The primary use case of
     * this function is to check if a [File] is a regular file, directory, or symbolic link.
     * @param flags A set of GFileQueryInfoFlags.
     * @return The GFileType of the [File], and *G_FILE_TYPE_UNKNOWN* if the file doesn't exist.
     */
    public fun queryFileType(flags: GFileQueryInfoFlags): GFileType =
        g_file_query_file_type(file = gFilePtr, cancellable = null, flags = flags)

    /**
     * Gets the requested information about specified file. The result is a [FileInfo] object that contains key-value
     * attributes (such as the type or size of the file). The [attributes] value is a String that specifies the file
     * attributes that should be gathered. It is not an error if it's not possible to read a particular requested
     * attribute from a file - it just won't be set. Attributes should be a comma-separated list of attributes, or
     * attribute wildcards. The wildcard "*" means **all** attributes, and a wildcard like "standard::*" means all
     * attributes in the standard namespace. An example attribute query would be "standard::*,owner::user". The
     * standard attributes are available as defines like `G_FILE_ATTRIBUTE_STANDARD_NAME`.
     *
     * If cancellable isn't *null* then the operation can be cancelled by triggering the cancellable object from
     * another thread. If the operation was cancelled the error `G_IO_ERROR_CANCELLED` will be returned. For symlinks
     * normally the information about the target of the symlink is returned, rather than information about the symlink
     * itself. However if you pass `G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS` in [flags] the information about the symlink
     * itself will be returned. Also for symlinks that point to non-existing files the information about the symlink
     * itself will be returned.
     *
     * If the file doesn't exist the `G_IO_ERROR_NOT_FOUND` error will be returned. Other errors are possible too, and
     * depend on what kind of filesystem the file is on.
     * @param attributes An attribute query String.
     * @param flags A set of GFileQueryInfoFlags.
     * @param error The [Error] instance to use for storing error information.
     * @return A [FileInfo] instance, or *null* if a error has occurred.
     */
    public fun queryInfo(attributes: String, flags: GFileQueryInfoFlags, error: Error): FileInfo? {
        val ptr = g_file_query_info(
            file = gFilePtr,
            attributes = attributes,
            flags = flags,
            error = cValuesOf(error.gErrorPtr),
            cancellable = null
        )
        return if (ptr != null) FileInfo.fromPointer(ptr) else null
    }

    public actual fun read(cancellable: Cancellable?, error: Error?): FileInputStream? {
        val ptr = g_file_read(
            file = gFilePtr,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr)
        )
        return if (ptr != null) FileInputStream.fromPointer(ptr) else null
    }

    /**
     * Gets an output stream for appending data to the file. If the file doesn't already exist it is created. By
     * default files created are generally readable by everyone, but if you pass `G_FILE_CREATE_PRIVATE` in [flags]
     * the file will be made readable only to the current user, to the level that is supported on the target
     * filesystem. If cancellable isn't *null* then the operation can be cancelled by triggering the cancellable
     * object from another thread. If the operation was cancelled then the error `G_IO_ERROR_CANCELLED` will be
     * returned.
     *
     * Some file systems don't allow all file names, and may return an `G_IO_ERROR_INVALID_FILENAME` error. If the
     * file is a directory the `G_IO_ERROR_IS_DIRECTORY` error will be returned. Other errors are possible too, and
     * depend on what kind of filesystem the file is on.
     * @param flags A set of flags to use.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return A [FileOutputStream] object, or *null* on error. Close the returned object with [FileOutputStream.close].
     */
    public fun appendTo(
        flags: GFileCreateFlags,
        cancellable: Cancellable? = null,
        error: Error? = null
    ): FileOutputStream? {
        val ptr = g_file_append_to(file = gFilePtr, flags = flags, cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr))
        return if (ptr != null) FileOutputStream.fromPointer(ptr) else null
    }

    /**
     * Creates a new file and returns an output stream for writing to it. The file **MUST NOT** already exist. By
     * default files created are generally readable by everyone, but if you pass `G_FILE_CREATE_PRIVATE` in [flags] the
     * file will be made readable only to the current user, to the level that is supported on the target filesystem.
     * If [cancellable] isn't *null* then the operation can be cancelled by triggering the cancellable object from
     * another thread. If the operation was cancelled then the error `G_IO_ERROR_CANCELLED` will be returned.
     *
     * If a file or directory with this name already exists the `G_IO_ERROR_EXISTS` error will be returned. Some file
     * systems don't allow all file names, and may return an `G_IO_ERROR_INVALID_FILENAME` error, and if the name is
     * too long then `G_IO_ERROR_FILENAME_TOO_LONG` will be returned. Other errors are possible too, and depend on
     * what kind of filesystem the file is on.
     * @param flags A set of flags to use.
     * @param cancellable The [Cancellable] to use or *null* to ignore.
     * @param error The [Error] to use or *null* to ignore.
     * @return A [FileOutputStream] object for the newly created file, or *null* on error. Close the returned object
     * with [FileOutputStream.close].
     */
    public fun create(
        flags: GFileCreateFlags,
        cancellable: Cancellable? = null,
        error: Error? = null
    ): FileOutputStream? {
        val ptr = g_file_create(file = gFilePtr, flags = flags, cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr))
        return if (ptr != null) FileOutputStream.fromPointer(ptr) else null
    }

    /**
     * Returns an output stream for overwriting the file, possibly creating a backup copy of the file first. If the
     * file doesn't exist then it will be created. This will try to replace the file in the safest way possible so
     * that any errors during the writing will not affect an already existing copy of the file. For instance, for
     * local files it may write to a temporary file and then atomically rename over the destination when the stream is
     * closed.
     *
     * By default files created are generally readable by everyone, but if you pass `G_FILE_CREATE_PRIVATE` in
     * [flags] the file will be made readable only to the current user, to the level that is supported on the target
     * filesystem. If [cancellable] isn't *null* then the operation can be cancelled by triggering the cancellable
     * object from another thread. However if the operation was cancelled then the error `G_IO_ERROR_CANCELLED` will be
     * returned.
     *
     * If you pass in a non-NULL [eTag] value and file already exists, then this value is compared to the current
     * entity tag of the file, and if they differ an `G_IO_ERROR_WRONG_ETAG` error is returned. This generally means
     * that the file has been changed since you last read it. You can get the new etag from
     * [FileOutputStream.eTag] after you've finished writing, and closed the [FileOutputStream]. When you load a new
     * file you can use [FileInputStream.queryInfo] to get the etag of the file.
     *
     * If [makeBackup] is *true* then this function will attempt to make a backup of the current file before
     * overwriting it. However if this fails a `G_IO_ERROR_CANT_CREATE_BACKUP` error will be returned. If you want to
     * replace anyway, try again with [makeBackup] set to *false*.
     *
     * If the file is a directory the `G_IO_ERROR_IS_DIRECTORY` error will be returned, and if the file is some other
     * form of non-regular file then a `G_IO_ERROR_NOT_REGULAR_FILE` error will be returned. Some file systems don't
     * allow all file names, and may return an `G_IO_ERROR_INVALID_FILENAME` error, and if the name is too long
     * `G_IO_ERROR_FILENAME_TOO_LONG` will be returned. Other errors are possible too, and depend on what kind of
     * filesystem the file is on.
     * @param flags The set of flags to use.
     * @param eTag An optional entity tag for the current file, or *""* (an empty [String]) to ignore.
     * @param makeBackup A value of *true* if a backup should be created.
     * @param cancellable The [Cancellable] to use, or *null* to ignore.
     * @param error The [Error] to use, or *null* to ignore.
     * @return A [FileOutputStream] object, or *null* on error. Close the returned object with
     * [FileOutputStream.close].
     */
    public fun replace(
        flags: GFileCreateFlags,
        eTag: String = "",
        makeBackup: Boolean = false,
        cancellable: Cancellable? = null,
        error: Error? = null
    ): FileOutputStream? {
        val ptr = g_file_replace(
            file = gFilePtr,
            etag = eTag.ifEmpty { null },
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr),
            make_backup = if (makeBackup) TRUE else FALSE,
            flags = flags
        )
        return if (ptr != null) FileOutputStream.fromPointer(ptr) else null
    }

    public actual fun delete(cancellable: Cancellable?, error: Error?): Boolean =
        g_file_delete(file = gFilePtr,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr)
        ) == TRUE

    public actual fun moveToTrash(cancellable: Cancellable?, error: Error?): Boolean =
        g_file_trash(
            file = gFilePtr,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr)
        ) == TRUE

    /**
     * Creates a new file and returns a stream for reading and writing to it. The file **MUST NOT** already exist. By
     * default files created are generally readable by everyone, but if you pass `G_FILE_CREATE_PRIVATE` in [flags]
     * the file will be made readable only to the current user, to the level that is supported on the target
     * filesystem. If cancellable isn't *null* then the operation can be cancelled by triggering the cancellable
     * object from another thread. If the operation was cancelled then the error `G_IO_ERROR_CANCELLED` will be
     * returned.
     *
     * If a file or directory with this name already exists then the `G_IO_ERROR_EXISTS` error will be returned. Some
     * file systems don't allow all file names, and may return an `G_IO_ERROR_INVALID_FILENAME` error, and if the name
     * is too long then `G_IO_ERROR_FILENAME_TOO_LONG` will be returned. Other errors are possible too, and depend on
     * what kind of filesystem the file is on.
     *
     * Note that in many non-local file cases read and write streams are not supported, so make sure you really need
     * to do read and write streaming, rather than just opening for reading or writing.
     * @param flags The set of flags to use.
     * @param cancellable The [Cancellable] to use, or *null* to ignore.
     * @param error The [Error] to use, or *null* to ignore.
     * @return A [FileOutputStream] object for the newly created file, or *null* on error. Close the returned object
     * with [FileIOStream.close].
     */
    public fun createWithReadWrite(
        flags: GFileCreateFlags,
        cancellable: Cancellable? = null,
        error: Error? = null
    ): FileIOStream? {
        val ptr = g_file_create_readwrite(file = gFilePtr, flags = flags, cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr))
        return if (ptr != null) FileIOStream.fromPointer(ptr) else null
    }

    /**
     * Opens an existing file for reading and writing. The result is a [FileIOStream] that can be used to read, and
     * write the contents of the file. If cancellable isn't *null* then the operation can be cancelled by triggering
     * the cancellable object from another thread. However if the operation was cancelled then the error
     * `G_IO_ERROR_CANCELLED` will be returned.
     *
     * If the file does not exist then the `G_IO_ERROR_NOT_FOUND` error will be returned. However if the file is a
     * directory then the `G_IO_ERROR_IS_DIRECTORY` error will be returned. Other errors are possible too, and depend
     * on what kind of filesystem the file is on. Note that in many non-local file cases read and write streams are
     * not supported, so make sure you really need to do read and write streaming, rather than just opening for
     * reading or writing.
     * @param cancellable The [Cancellable] to use, or *null* to ignore.
     * @param error The [Error] to use, or *null* to ignore.
     * @return A [FileIOStream] object, or *null* on error. Close the returned object with [FileIOStream.close].
     */
    public fun openWithReadWrite(cancellable: Cancellable? = null, error: Error? = null): FileIOStream? {
        val ptr = g_file_open_readwrite(file = gFilePtr, error = cValuesOf(error?.gErrorPtr),
            cancellable = cancellable?.gCancellablePtr)
        return if (ptr != null) FileIOStream.fromPointer(ptr) else null
    }

    /**
     * Returns an output stream for overwriting the file in read/write mode, possibly creating a backup copy of the
     * file first. If the file doesn't exist then it will be created. Note that in many non-local file cases read and
     * write streams are not supported, so make sure you really need to do read and write streaming, rather than
     * just opening for reading or writing.
     * @param flags The set of flags to use.
     * @param eTag An optional entity tag for the current file, or *""* (an empty [String]) to ignore.
     * @param makeBackup A value of *true* if a backup should be created.
     * @param cancellable The [Cancellable] to use, or *null* to ignore.
     * @param error The [Error] to use, or *null* to ignore.
     * * @return A [FileIOStream] object, or *null* on error. Close the returned object with [FileIOStream.close].
     * @see replace
     */
    public fun replaceWithReadWrite(
        flags: GFileCreateFlags,
        eTag: String = "",
        makeBackup: Boolean = false,
        cancellable: Cancellable? = null,
        error: Error? = null
    ): FileIOStream? {
        val ptr = g_file_replace_readwrite(
            file = gFilePtr,
            flags = flags,
            error = cValuesOf(error?.gErrorPtr),
            cancellable = cancellable?.gCancellablePtr,
            make_backup = if (makeBackup) TRUE else FALSE,
            etag = eTag.ifEmpty { null }
        )
        return if (ptr != null) FileIOStream.fromPointer(ptr) else null
    }
}
