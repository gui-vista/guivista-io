package io.gitlab.guiVista.io.file

import gio2.*
import glib2.TRUE
import glib2.g_object_unref
import glib2.gpointer
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer

public actual class FileMonitor private constructor(ptr: CPointer<GFileMonitor>?) : ObjectBase {
    public val gFileMonitorPtr: CPointer<GFileMonitor>? = ptr
    public actual val isCancelled: Boolean
        get() = g_file_monitor_is_cancelled(gFileMonitorPtr) == TRUE

    public actual fun cancel() {
        g_file_monitor_cancel(gFileMonitorPtr)
    }

    public actual fun changeRateLimit(limit: UInt) {
        g_file_monitor_set_rate_limit(gFileMonitorPtr, limit.toInt())
    }

    public companion object {
        public fun fromPointer(ptr: CPointer<GFileMonitor>?): FileMonitor = FileMonitor(ptr)
    }

    override fun close() {
        g_object_unref(gFileMonitorPtr)
    }

    /**
     * Connects the *changed* event to a [handler] on a [FileMonitor]. This event is used when **file** has been
     * changed. If using `G_FILE_MONITOR_WATCH_MOVES` on a directory monitor, and the information is available (and if
     * supported by the backend) then **eventType** may be `G_FILE_MONITOR_EVENT_RENAMED`, `G_FILE_MONITOR_EVENT_MOVED_IN`,
     * or `G_FILE_MONITOR_EVENT_MOVED_OUT`. In all cases **file** will be a child of the monitored directory. For
     * renames **file** will be the old name and **otherFile** is the new name. For "moved in" events **file** is the
     * name of the file that appeared, and **otherFile** is the old name that it was moved from (in another directory).
     * For "moved out" events **file** is the name of the file that used to be in this directory, and **otherFile** is
     * the name of the file at its new location.
     *
     * It makes sense to treat `G_FILE_MONITOR_EVENT_MOVED_IN` as equivalent to `G_FILE_MONITOR_EVENT_CREATED`, and
     * `G_FILE_MONITOR_EVENT_MOVED_OUT` as equivalent to `G_FILE_MONITOR_EVENT_DELETED`, with extra information.
     * Note that `G_FILE_MONITOR_EVENT_RENAMED` is equivalent to a delete/create pair. This is exactly how the events
     * will be reported in the case that the `G_FILE_MONITOR_WATCH_MOVES` flag is not in use.
     *
     * If using the deprecated flag `G_FILE_MONITOR_SEND_MOVED` flag, and **eventType** is
     * `G_FILE_MONITOR_EVENT_MOVED` then **file** will be set to a [File] containing the old path, and **otherFile**
     * will be set to a [File] containing the new path. In all the other cases **otherFile** will be set to *null*.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectChangedEvent(
        handler: CPointer<ChangedHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gFileMonitorPtr, signal = FileMonitorEvent.changed, slot = handler, data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gFileMonitorPtr, handlerId)
    }
}

/**
 * The event handler for the *changed* event. Arguments:
 * 1. monitor: CPointer<GFileMonitor>
 * 2. file: CPointer<GFile>
 * 3. otherFile: CPointer<GFile>?
 * 4. eventType: GFileMonitorEvent
 * 5. userData: gpointer
 */
public typealias ChangedHandler = CFunction<(
    monitor: CPointer<GFileMonitor>,
    file: CPointer<GFile>,
    otherFile: CPointer<GFile>?,
    eventType: GFileMonitorEvent,
    userData: gpointer
) -> Unit>
