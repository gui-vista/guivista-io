package io.gitlab.guiVista.io.stream.output

import gio2.GBufferedOutputStream
import gio2.g_buffered_output_stream_get_auto_grow
import gio2.g_buffered_output_stream_get_buffer_size
import gio2.g_buffered_output_stream_set_auto_grow
import glib2.FALSE
import glib2.TRUE
import kotlinx.cinterop.CPointer

public actual interface BufferedOutputStreamBase : FilterOutputStream {
    public val gBufferedOutputStreamPtr: CPointer<GBufferedOutputStream>?
    public actual val bufferSize: ULong
        get() = g_buffered_output_stream_get_buffer_size(gBufferedOutputStreamPtr)
    public actual var autoGrow: Boolean
        get() = g_buffered_output_stream_get_auto_grow(gBufferedOutputStreamPtr) == TRUE
        set(value) = g_buffered_output_stream_set_auto_grow(gBufferedOutputStreamPtr, if (value) TRUE else FALSE)
}
