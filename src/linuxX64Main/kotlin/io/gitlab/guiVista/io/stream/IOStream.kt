package io.gitlab.guiVista.io.stream

import gio2.GIOStream
import glib2.g_object_unref
import kotlinx.cinterop.CPointer

public actual class IOStream private constructor(ptr: CPointer<GIOStream>?) : IOStreamBase {
    override val gIOStreamPtr: CPointer<GIOStream>? = ptr

    public companion object {
        public fun fromPointer(ptr: CPointer<GIOStream>?): IOStream = IOStream(ptr)
    }

    override fun close() {
        g_object_unref(gIOStreamPtr)
    }
}
