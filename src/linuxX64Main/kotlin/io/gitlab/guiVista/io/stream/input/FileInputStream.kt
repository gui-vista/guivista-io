package io.gitlab.guiVista.io.stream.input

import gio2.GFileInputStream
import gio2.GInputStream
import gio2.GSeekable
import gio2.g_file_input_stream_query_info
import glib2.g_object_unref
import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.io.Cancellable
import io.gitlab.guiVista.io.file.FileInfo
import io.gitlab.guiVista.io.stream.Seekable
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.cValuesOf
import kotlinx.cinterop.reinterpret

public actual class FileInputStream private constructor(ptr: CPointer<GFileInputStream>?) : InputStreamBase, Seekable {
    public val gFileInputStreamPtr: CPointer<GFileInputStream>? = ptr
    override val gInputStreamPtr: CPointer<GInputStream>?
        get() = gFileInputStreamPtr?.reinterpret()
    override val gSeekablePtr: CPointer<GSeekable>?
        get() = gFileInputStreamPtr?.reinterpret()

    public companion object {
        public fun fromPointer(ptr: CPointer<GFileInputStream>?): FileInputStream = FileInputStream(ptr)
    }

    override fun close() {
        g_object_unref(gFileInputStreamPtr)
    }

    /**
     * Queries a file input stream the given [attributes]. This function blocks while querying the stream. For the
     * asynchronous (non-blocking) version of this function, see `g_file_input_stream_query_info_async()`. While the
     * stream is blocked, the stream will set the pending flag internally, and any other operations on the stream will
     * fail with `G_IO_ERROR_PENDING`.
     * @param attributes A file attribute query [String].
     * @param cancellable The [Cancellable] to use or *null*.
     * @param error The [Error] to use or *null*.
     * @return A instance of [FileInfo] or *null* on error.
     */
    public actual fun queryInfo(attributes: String, cancellable: Cancellable?, error: Error?): FileInfo? {
        val ptr = g_file_input_stream_query_info(stream = gFileInputStreamPtr,
            attributes = attributes,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr)
        )
        return if (ptr != null) FileInfo.fromPointer(ptr) else null
    }
}
