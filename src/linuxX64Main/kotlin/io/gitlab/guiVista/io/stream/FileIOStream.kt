package io.gitlab.guiVista.io.stream

import gio2.*
import glib2.g_object_unref
import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.io.Cancellable
import io.gitlab.guiVista.io.file.FileInfo
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.cValuesOf
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString

public actual class FileIOStream private constructor(ptr: CPointer<GFileIOStream>?) : IOStreamBase, Seekable {
    public val gFileIOStreamPtr: CPointer<GFileIOStream>? = ptr
    override val gIOStreamPtr: CPointer<GIOStream>?
        get() = gFileIOStreamPtr?.reinterpret()
    override val gSeekablePtr: CPointer<GSeekable>?
        get() = gFileIOStreamPtr?.reinterpret()
    public actual val eTag: String
        get() = g_file_io_stream_get_etag(gFileIOStreamPtr)?.toKString() ?: ""

    public companion object {
        public fun fromPointer(ptr: CPointer<GFileIOStream>?): FileIOStream = FileIOStream(ptr)
    }

    override fun close() {
        g_object_unref(gFileIOStreamPtr)
    }

    public actual fun queryInfo(attributes: String, cancellable: Cancellable?, error: Error?): FileInfo? {
        val ptr = g_file_io_stream_query_info(
            stream = gFileIOStreamPtr,
            attributes = attributes,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr)
        )
        return if (ptr != null) FileInfo.fromPointer(ptr) else null
    }
}
