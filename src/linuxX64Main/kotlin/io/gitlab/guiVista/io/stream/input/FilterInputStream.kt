package io.gitlab.guiVista.io.stream.input

import gio2.GFilterInputStream
import kotlinx.cinterop.CPointer

public actual interface FilterInputStream : InputStreamBase {
    public val gFilterInputStreamPtr: CPointer<GFilterInputStream>?
    public actual val baseStream: InputStreamBase
    public actual var closeBaseStream: Boolean
}
