package io.gitlab.guiVista.io.stream.input

import gio2.GPollableInputStream
import gio2.g_pollable_input_stream_can_poll
import gio2.g_pollable_input_stream_is_readable
import gio2.g_pollable_input_stream_read_nonblocking
import glib2.TRUE
import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.io.Cancellable
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.addressOf
import kotlinx.cinterop.cValuesOf
import kotlinx.cinterop.usePinned

public actual interface PollableInputStream {
    public val gPollableInputStreamPtr: CPointer<GPollableInputStream>?
    public actual val canPoll: Boolean
        get() = g_pollable_input_stream_can_poll(gPollableInputStreamPtr) == TRUE
    public actual val isReadable: Boolean
        get() = g_pollable_input_stream_is_readable(gPollableInputStreamPtr) == TRUE

    public actual fun readNonBlocking(count: ULong, cancellable: Cancellable?, error: Error?): Pair<Long, ByteArray> {
        val buffer = ByteArray(count.toInt())
        var bytesRead = 0L
        buffer.usePinned { pinned ->
            bytesRead = g_pollable_input_stream_read_nonblocking(
                stream = gPollableInputStreamPtr,
                count = count,
                error = cValuesOf(error?.gErrorPtr),
                cancellable = cancellable?.gCancellablePtr,
                buffer = pinned.addressOf(0)
            )
        }
        return bytesRead to buffer
    }
}
