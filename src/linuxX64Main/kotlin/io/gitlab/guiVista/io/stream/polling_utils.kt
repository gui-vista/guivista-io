package io.gitlab.guiVista.io.stream

import gio2.*
import glib2.FALSE
import glib2.GSource
import glib2.TRUE
import glib2.gpointer
import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.Object
import io.gitlab.guiVista.io.Cancellable
import io.gitlab.guiVista.io.stream.input.InputStreamBase
import io.gitlab.guiVista.io.stream.output.OutputStreamBase
import kotlinx.cinterop.*

public actual class PollableSource private constructor(ptr: CPointer<GSource>?) {
    public val gSourcePtr: CPointer<GSource>? = ptr

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GSource>?): PollableSource = PollableSource(ptr)

        public actual fun create(pollableStream: Object): PollableSource =
            fromPointer(g_pollable_source_new(pollableStream.objPtr?.reinterpret()))

        public fun createWithChildSource(
            pollableStream: gpointer?,
            childSrc: PollableSource? = null,
            cancellable: Cancellable? = null
        ): PollableSource = fromPointer(
            g_pollable_source_new_full(
                pollable_stream = pollableStream,
                child_source = childSrc?.gSourcePtr,
                cancellable = cancellable?.gCancellablePtr
            )
        )
    }
}

public actual fun readFromStream(
    inputStream: InputStreamBase,
    count: ULong,
    blocking: Boolean,
    cancellable: Cancellable?,
    error: Error?
): Pair<Long, ByteArray> {
    val buffer = ByteArray(count.toInt())
    var bytesRead = 0L
    buffer.usePinned { pinned ->
        bytesRead = g_pollable_stream_read(
            stream = inputStream.gInputStreamPtr,
            count = count,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr),
            blocking = if (blocking) TRUE else FALSE,
            buffer = pinned.addressOf(0)
        )
    }
    return bytesRead to buffer
}

public actual fun writeToStream(
    outputStream: OutputStreamBase,
    buffer: ByteArray,
    blocking: Boolean,
    cancellable: Cancellable?,
    error: Error?
): Long {
    var bytesWritten = 0L
    buffer.usePinned { pinned ->
        bytesWritten = g_pollable_stream_write(
            stream = outputStream.gOutputStreamPtr,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr),
            blocking = if (blocking) TRUE else FALSE,
            count = buffer.size.toULong(),
            buffer = pinned.addressOf(0)
        )
    }
    return bytesWritten
}

public actual fun writeAllToStream(
    outputStream: OutputStreamBase,
    buffer: ByteArray,
    blocking: Boolean,
    cancellable: Cancellable?,
    error: Error?
): Pair<Boolean, ULong> = memScoped {
    val bytesWritten = alloc<ULongVar>()
    var success = false
    buffer.usePinned { pinned ->
        success = g_pollable_stream_write_all(
            stream = outputStream.gOutputStreamPtr,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr),
            bytes_written = bytesWritten.ptr,
            count = buffer.size.toULong(),
            blocking = if (blocking) TRUE else FALSE,
            buffer = pinned.addressOf(0)
        ) == TRUE
    }
    success to bytesWritten.value.toULong()
}
