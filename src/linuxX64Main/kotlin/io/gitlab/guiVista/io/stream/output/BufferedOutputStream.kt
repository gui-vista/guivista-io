package io.gitlab.guiVista.io.stream.output

import gio2.*
import glib2.FALSE
import glib2.TRUE
import glib2.g_object_unref
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class BufferedOutputStream private constructor(
    ptr: CPointer<GBufferedOutputStream>?
) : BufferedOutputStreamBase {
    override val gBufferedOutputStreamPtr: CPointer<GBufferedOutputStream>? = ptr
    override val gFilterOutputStreamPtr: CPointer<GFilterOutputStream>?
        get() = gBufferedOutputStreamPtr?.reinterpret()
    override val gOutputStreamPtr: CPointer<GOutputStream>?
        get() = gBufferedOutputStreamPtr?.reinterpret()
    override val baseStream: OutputStreamBase
        get() = OutputStream.fromPointer(g_filter_output_stream_get_base_stream(gFilterOutputStreamPtr))
    override var closeBaseStream: Boolean
        get() = g_filter_output_stream_get_close_base_stream(gFilterOutputStreamPtr) == TRUE
        set(value) = g_filter_output_stream_set_close_base_stream(gFilterOutputStreamPtr, if (value) TRUE else FALSE)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GBufferedOutputStream>?): BufferedOutputStream = BufferedOutputStream(ptr)

        public actual fun create(baseStream: OutputStreamBase, size: ULong): BufferedOutputStream {
            val ptr = if (size == 0uL) {
                g_buffered_output_stream_new(baseStream.gOutputStreamPtr)
            } else {
                g_buffered_output_stream_new_sized(baseStream.gOutputStreamPtr, size)
            }
            return fromPointer(ptr?.reinterpret())
        }
    }

    override fun close() {
        g_object_unref(gBufferedOutputStreamPtr)
    }
}
