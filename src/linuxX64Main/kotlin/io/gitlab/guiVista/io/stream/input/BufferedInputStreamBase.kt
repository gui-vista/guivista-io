package io.gitlab.guiVista.io.stream.input

import gio2.*
import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.io.Cancellable
import kotlinx.cinterop.*

public actual interface BufferedInputStreamBase : FilterInputStream {
    public val gBufferedInputStreamPtr: CPointer<GBufferedInputStream>?
    public actual var bufferSize: ULong
        get() = g_buffered_input_stream_get_buffer_size(gBufferedInputStreamPtr)
        set(value) = g_buffered_input_stream_set_buffer_size(gBufferedInputStreamPtr, value)
    public actual val availableDataSize: ULong
        get() = g_buffered_input_stream_get_available(gBufferedInputStreamPtr)

    public actual fun peek(): Pair<ULong, ByteArray> = memScoped {
        val count = alloc<ULongVar>()
        val ptr = g_buffered_input_stream_peek_buffer(gBufferedInputStreamPtr, count.ptr)
        val buffer = ByteArray(count.value.toInt())
        val tmpBuffer = ptr?.reinterpret<ByteVar>()
        var pos = 0
        var item: Byte?
        do {
            item = tmpBuffer?.get(pos)
            pos++
        } while (item != null)
        count.value.toULong() to buffer
    }

    public actual fun peekAndCopy(buffer: ByteArray, count: ULong, offset: ULong) {
        buffer.usePinned { pinned ->
            g_buffered_input_stream_peek(
                stream = gBufferedInputStreamPtr,
                offset = offset,
                count = count,
                buffer = pinned.addressOf(0)
            )
        }
    }

    public actual fun fill(count: Long, cancellable: Cancellable?, error: Error?): Long =
        g_buffered_input_stream_fill(
            stream = gBufferedInputStreamPtr,
            count = count,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr)
        )

    public actual fun readByte(cancellable: Cancellable?, error: Error?): Int =
        g_buffered_input_stream_read_byte(
            stream = gBufferedInputStreamPtr,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr)
        )
}
