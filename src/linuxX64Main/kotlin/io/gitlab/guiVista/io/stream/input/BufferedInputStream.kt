package io.gitlab.guiVista.io.stream.input

import gio2.*
import glib2.FALSE
import glib2.TRUE
import glib2.g_object_unref
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class BufferedInputStream private constructor(
    ptr: CPointer<GBufferedInputStream>?
) : BufferedInputStreamBase {
    override val gBufferedInputStreamPtr: CPointer<GBufferedInputStream>? = ptr
    override val gFilterInputStreamPtr: CPointer<GFilterInputStream>?
        get() = gBufferedInputStreamPtr?.reinterpret()
    override val gInputStreamPtr: CPointer<GInputStream>?
        get() = gBufferedInputStreamPtr?.reinterpret()
    override val baseStream: InputStreamBase
        get() = InputStream.fromPointer(g_filter_input_stream_get_base_stream(gFilterInputStreamPtr))
    override var closeBaseStream: Boolean
        get() = g_filter_input_stream_get_close_base_stream(gFilterInputStreamPtr) == TRUE
        set(value) = g_filter_input_stream_set_close_base_stream(gFilterInputStreamPtr, if (value) TRUE else FALSE)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GBufferedInputStream>?): BufferedInputStream = BufferedInputStream(ptr)

        public actual fun create(baseStream: InputStreamBase, size: ULong): BufferedInputStream {
            val ptr = if (size == 0uL) {
                g_buffered_input_stream_new(baseStream.gInputStreamPtr)
            } else {
                g_buffered_input_stream_new_sized(baseStream.gInputStreamPtr, size)
            }
            return fromPointer(ptr?.reinterpret())
        }
    }

    override fun close() {
        g_object_unref(gBufferedInputStreamPtr)
    }
}
