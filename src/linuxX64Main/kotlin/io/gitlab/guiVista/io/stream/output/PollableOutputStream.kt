package io.gitlab.guiVista.io.stream.output

import gio2.GPollableOutputStream
import gio2.g_pollable_output_stream_can_poll
import gio2.g_pollable_output_stream_is_writable
import gio2.g_pollable_output_stream_write_nonblocking
import glib2.TRUE
import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.io.Cancellable
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.addressOf
import kotlinx.cinterop.cValuesOf
import kotlinx.cinterop.usePinned

public actual interface PollableOutputStream {
    public val gPollableOutputStreamPtr: CPointer<GPollableOutputStream>?
    public actual val canPoll: Boolean
        get() = g_pollable_output_stream_can_poll(gPollableOutputStreamPtr) == TRUE
    public actual val isWritable: Boolean
        get() = g_pollable_output_stream_is_writable(gPollableOutputStreamPtr) == TRUE

    public actual fun writeNonBlocking(buffer: ByteArray, cancellable: Cancellable?, error: Error?): Long {
        var result = 0L
        buffer.usePinned { pinned ->
            result = g_pollable_output_stream_write_nonblocking(
                stream = gPollableOutputStreamPtr,
                cancellable = cancellable?.gCancellablePtr,
                error = cValuesOf(error?.gErrorPtr),
                count = buffer.size.toULong(),
                buffer = pinned.addressOf(0)
            )
        }
        return result
    }
}
