package io.gitlab.guiVista.io.stream.input

import gio2.GInputStream
import glib2.g_object_unref
import kotlinx.cinterop.CPointer

public actual class InputStream private constructor(ptr: CPointer<GInputStream>?) : InputStreamBase {
    override val gInputStreamPtr: CPointer<GInputStream>? = ptr

    public companion object {
        public fun fromPointer(ptr: CPointer<GInputStream>?): InputStream = InputStream(ptr)
    }

    override fun close() {
        g_object_unref(gInputStreamPtr)
    }
}
