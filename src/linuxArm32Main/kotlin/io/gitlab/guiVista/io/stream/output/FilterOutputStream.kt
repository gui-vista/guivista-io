package io.gitlab.guiVista.io.stream.output

import gio2.GFilterOutputStream
import kotlinx.cinterop.CPointer

public actual interface FilterOutputStream : OutputStreamBase {
    public val gFilterOutputStreamPtr: CPointer<GFilterOutputStream>?
    public actual val baseStream: OutputStreamBase
    public actual var closeBaseStream: Boolean
}
