package io.gitlab.guiVista.io.stream.output

import gio2.*
import glib2.TRUE
import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.io.Cancellable
import kotlinx.cinterop.*

public actual interface OutputStreamBase : ObjectBase {
    public val gOutputStreamPtr: CPointer<GOutputStream>?
    public actual val streamIsClosing: Boolean
        get() = g_output_stream_is_closing(gOutputStreamPtr) == TRUE
    public actual val streamIsClosed: Boolean
        get() = g_output_stream_is_closed(gOutputStreamPtr) == TRUE
    public actual val hasPending: Boolean
        get() = g_output_stream_has_pending(gOutputStreamPtr) == TRUE

    public actual fun changePending(error: Error?): Boolean =
        g_output_stream_set_pending(gOutputStreamPtr, cValuesOf(error?.gErrorPtr)) == TRUE

    public actual fun clearPending() {
        g_output_stream_clear_pending(gOutputStreamPtr)
    }

    public actual fun write(buffer: ByteArray, cancellable: Cancellable?, error: Error?): Long =
        buffer.usePinned { pinned ->
            g_output_stream_write(
                stream = gOutputStreamPtr,
                cancellable = cancellable?.gCancellablePtr,
                error = cValuesOf(error?.gErrorPtr),
                count = buffer.size.toUInt(),
                buffer = pinned.addressOf(0)
            )
        }.toLong()

    public actual fun writeAll(
        buffer: ByteArray,
        cancellable: Cancellable?,
        error: Error?
    ): Pair<ULong, Boolean> = memScoped {
        val bytesWritten = alloc<UIntVar>()
        var success = false
        buffer.usePinned { pinned ->
            success = g_output_stream_write_all(
                stream = gOutputStreamPtr,
                cancellable = cancellable?.gCancellablePtr,
                error = cValuesOf(error?.gErrorPtr),
                count = buffer.size.toUInt(),
                bytes_written = bytesWritten.ptr,
                buffer = pinned.addressOf(0)
            ) == TRUE
        }
        bytesWritten.value.toULong() to success
    }

    public actual fun flush(cancellable: Cancellable?, error: Error?) {
        g_output_stream_flush(stream = gOutputStreamPtr, cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr))
    }

    public actual fun closeStream(cancellable: Cancellable?, error: Error?): Boolean =
        g_output_stream_close(stream = gOutputStreamPtr, cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr)) == TRUE
}
