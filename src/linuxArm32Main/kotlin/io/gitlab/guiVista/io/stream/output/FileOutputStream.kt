package io.gitlab.guiVista.io.stream.output

import gio2.GFileOutputStream
import gio2.GOutputStream
import gio2.g_file_output_stream_get_etag
import gio2.g_file_output_stream_query_info
import glib2.g_object_unref
import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.io.Cancellable
import io.gitlab.guiVista.io.file.FileInfo
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.cValuesOf
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString

public actual class FileOutputStream private constructor(ptr: CPointer<GFileOutputStream>?) : OutputStreamBase {
    public val gFileOutputStreamPtr: CPointer<GFileOutputStream>? = ptr
    override val gOutputStreamPtr: CPointer<GOutputStream>?
        get() = gFileOutputStreamPtr?.reinterpret()
    public actual val eTag: String
        get() = g_file_output_stream_get_etag(gFileOutputStreamPtr)?.toKString() ?: ""

    public companion object {
        public fun fromPointer(ptr: CPointer<GFileOutputStream>?): FileOutputStream = FileOutputStream(ptr)
    }

    public actual fun queryInfo(attributes: String, cancellable: Cancellable?, error: Error?): FileInfo? {
        val ptr = g_file_output_stream_query_info(
            stream = gFileOutputStreamPtr,
            attributes = attributes,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr)
        )
        return if (ptr != null) FileInfo.fromPointer(ptr) else null
    }

    override fun close() {
        g_object_unref(gFileOutputStreamPtr)
    }
}
