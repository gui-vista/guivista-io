package io.gitlab.guiVista.io.stream.output

import gio2.GOutputStream
import glib2.g_object_unref
import kotlinx.cinterop.CPointer

public actual class OutputStream private constructor(ptr: CPointer<GOutputStream>?) : OutputStreamBase {
    override val gOutputStreamPtr: CPointer<GOutputStream>? = ptr

    public companion object {
        public fun fromPointer(ptr: CPointer<GOutputStream>?): OutputStream = OutputStream(ptr)
    }

    override fun close() {
        g_object_unref(gOutputStreamPtr)
    }
}
