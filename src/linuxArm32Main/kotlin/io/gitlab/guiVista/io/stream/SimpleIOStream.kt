package io.gitlab.guiVista.io.stream

import gio2.GIOStream
import gio2.GSimpleIOStream
import gio2.g_simple_io_stream_new
import glib2.g_object_unref
import io.gitlab.guiVista.io.stream.input.InputStreamBase
import io.gitlab.guiVista.io.stream.output.OutputStreamBase
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class SimpleIOStream private constructor(
    ptr: CPointer<GSimpleIOStream>? = null,
    inputStream: InputStreamBase? = null,
    outputStream: OutputStreamBase? = null
) : IOStreamBase {
    public val gSimpleIOStreamPtr: CPointer<GSimpleIOStream>? = ptr
        ?: g_simple_io_stream_new(inputStream?.gInputStreamPtr, outputStream?.gOutputStreamPtr)?.reinterpret()
    override val gIOStreamPtr: CPointer<GIOStream>?
        get() = gSimpleIOStreamPtr?.reinterpret()

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GSimpleIOStream>?): SimpleIOStream = SimpleIOStream(ptr = ptr)

        public actual fun create(inputStream: InputStreamBase, outputStream: OutputStreamBase): SimpleIOStream =
            SimpleIOStream(inputStream = inputStream, outputStream = outputStream)
    }

    override fun close() {
        g_object_unref(gSimpleIOStreamPtr)
    }
}
