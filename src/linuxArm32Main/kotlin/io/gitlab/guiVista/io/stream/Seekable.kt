package io.gitlab.guiVista.io.stream

import gio2.*
import glib2.GSeekType
import glib2.TRUE
import io.gitlab.guiVista.core.Error
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.cValuesOf

public actual interface Seekable {
    public val gSeekablePtr: CPointer<GSeekable>?
    public actual val canSeek: Boolean
        get() = g_seekable_can_seek(gSeekablePtr) == TRUE
    public actual val canTruncate: Boolean
        get() = g_seekable_can_truncate(gSeekablePtr) == TRUE

    public actual fun tell(): Long = g_seekable_tell(gSeekablePtr)

    public actual fun truncate(offset: Long, error: Error?): Boolean =
        g_seekable_truncate(
            seekable = gSeekablePtr,
            error = cValuesOf(error?.gErrorPtr),
            offset = offset,
            cancellable = null
        ) == TRUE

    /**
     * Seeks in the stream by the given [offset], modified by [type]. Attempting to seek past the end of the stream
     * will have different results depending on if the stream is fixed sized or resizable. If the stream is resizable
     * then seeking past the end, and then writing will result in zeros filling the empty space. Seeking past the end
     * of a resizable stream, and reading will result in end of file. Seeking past the end of a fixed sized stream will
     * fail.
     *
     * Any operation that would result in a negative offset will fail. If the operation was cancelled then the error
     * `G_IO_ERROR_CANCELLED` will be returned.
     * @param offset The offset.
     * @param type The seek type to use.
     * @param error The [Error] to use for storing the error, or *null* to ignore.
     * @return A value of *true* if successful. If an error has occurred then this function will return *false*, and
     * set error appropriately if present.
     */
    public fun seek(offset: Long, type: GSeekType, error: Error? = null): Boolean =
        g_seekable_seek(
            seekable = gSeekablePtr,
            offset = offset,
            cancellable = null,
            error = cValuesOf(error?.gErrorPtr),
            type = type
        ) == TRUE
}
