package io.gitlab.guiVista.io.stream.input

import gio2.*
import glib2.TRUE
import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.io.Cancellable
import kotlinx.cinterop.*

public actual interface InputStreamBase : ObjectBase {
    public val gInputStreamPtr: CPointer<GInputStream>?
    public actual val streamIsClosed: Boolean
        get() = g_input_stream_is_closed(gInputStreamPtr) == TRUE
    public actual val hasPending: Boolean
        get() = g_input_stream_has_pending(gInputStreamPtr) == TRUE

    public actual fun read(count: ULong, cancellable: Cancellable?, error: Error?): Pair<ByteArray, Long> {
        val buffer = ByteArray(count.toInt())
        var bytesRead = 0L
        buffer.usePinned { pinned ->
            bytesRead = g_input_stream_read(
                stream = gInputStreamPtr,
                count = count.toUInt(),
                cancellable = cancellable?.gCancellablePtr,
                error = cValuesOf(error?.gErrorPtr),
                buffer = pinned.addressOf(0)
            ).toLong()
        }
        return buffer to bytesRead
    }

    public actual fun changePending(error: Error?): Boolean =
        g_input_stream_set_pending(gInputStreamPtr, cValuesOf(error?.gErrorPtr)) == TRUE

    public actual fun clearPending() {
        g_input_stream_clear_pending(gInputStreamPtr)
    }

    public actual fun readAll(
        count: ULong,
        cancellable: Cancellable?,
        error: Error?
    ): Triple<ByteArray, ULong, Boolean> = memScoped {
        val buffer = ByteArray(count.toInt())
        val bytesRead = alloc<UIntVar>()
        var success = false
        buffer.usePinned { pinned ->
            success = g_input_stream_read_all(
                stream = gInputStreamPtr,
                error = cValuesOf(error?.gErrorPtr),
                cancellable = cancellable?.gCancellablePtr,
                count = count.toUInt(),
                bytes_read = bytesRead.ptr,
                buffer = pinned.addressOf(0)
            ) == TRUE
        }
        Triple(buffer, bytesRead.value.toULong(), success)
    }

    public actual fun skip(count: ULong, cancellable: Cancellable?, error: Error?): Long =
        g_input_stream_skip(
            stream = gInputStreamPtr,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr),
            count = count.toUInt()
        ).toLong()

    public actual fun closeStream(cancellable: Cancellable?, error: Error?): Boolean =
        g_input_stream_close(stream = gInputStreamPtr, cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr)) == TRUE
}
