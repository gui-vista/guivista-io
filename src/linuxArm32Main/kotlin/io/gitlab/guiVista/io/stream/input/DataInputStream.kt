package io.gitlab.guiVista.io.stream.input

import gio2.*
import glib2.FALSE
import glib2.TRUE
import glib2.g_object_unref
import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.io.Cancellable
import io.gitlab.guiVista.io.stream.Seekable
import kotlinx.cinterop.*

public actual class DataInputStream private constructor(
    ptr: CPointer<GDataInputStream>?
) : BufferedInputStreamBase, Seekable {
    public val gDataInputStream: CPointer<GDataInputStream>? = ptr
    override val gBufferedInputStreamPtr: CPointer<GBufferedInputStream>?
        get() = gDataInputStream?.reinterpret()
    override val gFilterInputStreamPtr: CPointer<GFilterInputStream>?
        get() = gDataInputStream?.reinterpret()
    override val gInputStreamPtr: CPointer<GInputStream>?
        get() = gDataInputStream?.reinterpret()
    override val gSeekablePtr: CPointer<GSeekable>?
        get() = gDataInputStream?.reinterpret()
    override val baseStream: InputStreamBase
        get() = InputStream.fromPointer(g_filter_input_stream_get_base_stream(gFilterInputStreamPtr))
    override var closeBaseStream: Boolean
        get() = g_filter_input_stream_get_close_base_stream(gFilterInputStreamPtr) == TRUE
        set(value) = g_filter_input_stream_set_close_base_stream(gFilterInputStreamPtr, if (value) TRUE else FALSE)

    /**
     * The byte order. Default value is `G_DATA_STREAM_BYTE_ORDER_BIG_ENDIAN`.
     *
     * Data binding property name: **byte-order**
     */
    public var byteOrder: GDataStreamByteOrder
        get() = g_data_input_stream_get_byte_order(gDataInputStream)
        set(value) = g_data_input_stream_set_byte_order(gDataInputStream, value)

    /**
     * The accepted types of line ending. Default value is `G_DATA_STREAM_NEWLINE_TYPE_LF`.
     *
     * Data binding property name: **newline-type**
     */
    public var newlineType: GDataStreamNewlineType
        get() = g_data_input_stream_get_newline_type(gDataInputStream)
        set(value) = g_data_input_stream_set_newline_type(gDataInputStream, value)

    override fun close() {
        g_object_unref(gDataInputStream)
    }

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GDataInputStream>?): DataInputStream = DataInputStream(ptr)

        public actual fun create(baseStream: InputStreamBase): DataInputStream =
            fromPointer(g_data_input_stream_new(baseStream.gInputStreamPtr))
    }

    public actual fun readUByte(cancellable: Cancellable?, error: Error?): UByte =
        g_data_input_stream_read_byte(
            stream = gDataInputStream,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr)
        )

    public actual fun readShort(cancellable: Cancellable?, error: Error?): Short =
        g_data_input_stream_read_int16(
            stream = gDataInputStream,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr)
        )

    public actual fun readUShort(cancellable: Cancellable?, error: Error?): UShort =
        g_data_input_stream_read_uint16(
            stream = gDataInputStream,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr)
        )

    public actual fun readInt(cancellable: Cancellable?, error: Error?): Int =
        g_data_input_stream_read_int32(
            stream = gDataInputStream,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr)
        )

    public actual fun readUInt(cancellable: Cancellable?, error: Error?): UInt =
        g_data_input_stream_read_uint32(
            stream = gDataInputStream,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr)
        )

    public actual fun readLong(cancellable: Cancellable?, error: Error?): Long =
        g_data_input_stream_read_int64(
            stream = gDataInputStream,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr)
        )

    public actual fun readULong(cancellable: Cancellable?, error: Error?): ULong =
        g_data_input_stream_read_uint64(
            stream = gDataInputStream,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr)
        )

    public actual fun readLine(cancellable: Cancellable?, error: Error?): Pair<ULong, String?> = memScoped {
        val length = alloc<UIntVar>()
        val data = g_data_input_stream_read_line(
            stream = gDataInputStream,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr),
            length = length.ptr
        )
        length.value.toULong() to data?.toKString()
    }

    public actual fun readUtf8Line(cancellable: Cancellable?, error: Error?): Pair<ULong, String?> = memScoped {
        val length = alloc<UIntVar>()
        val data = g_data_input_stream_read_line_utf8(
            stream = gDataInputStream,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr),
            length = length.ptr
        )
        length.value.toULong() to data?.toKString()
    }

    public actual fun readUpto(
        stopChars: String,
        cancellable: Cancellable?,
        error: Error?
    ): Pair<ULong, String?> = memScoped {
        val length = alloc<UIntVar>()
        val data = g_data_input_stream_read_upto(
            stream = gDataInputStream,
            length = length.ptr,
            cancellable = cancellable?.gCancellablePtr,
            stop_chars = stopChars,
            error = cValuesOf(error?.gErrorPtr),
            stop_chars_len = stopChars.length
        )
        length.value.toULong() to data?.toKString()
    }
}
