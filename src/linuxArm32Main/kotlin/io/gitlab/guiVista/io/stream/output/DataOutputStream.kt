package io.gitlab.guiVista.io.stream.output

import gio2.*
import glib2.FALSE
import glib2.TRUE
import glib2.g_object_unref
import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.io.Cancellable
import io.gitlab.guiVista.io.stream.Seekable
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.cValuesOf
import kotlinx.cinterop.reinterpret

public actual class DataOutputStream private constructor(
    ptr: CPointer<GDataOutputStream>?
) : FilterOutputStream, Seekable {
    public val gDataOutputStreamPtr: CPointer<GDataOutputStream>? = ptr
    override val gFilterOutputStreamPtr: CPointer<GFilterOutputStream>?
        get() = gDataOutputStreamPtr?.reinterpret()
    override val gOutputStreamPtr: CPointer<GOutputStream>?
        get() = gDataOutputStreamPtr?.reinterpret()
    override val gSeekablePtr: CPointer<GSeekable>?
        get() = gDataOutputStreamPtr?.reinterpret()
    override val baseStream: OutputStreamBase
        get() = OutputStream.fromPointer(g_filter_output_stream_get_base_stream(gFilterOutputStreamPtr))
    override var closeBaseStream: Boolean
        get() = g_filter_output_stream_get_close_base_stream(gFilterOutputStreamPtr) == TRUE
        set(value) = g_filter_output_stream_set_close_base_stream(gFilterOutputStreamPtr, if (value) TRUE else FALSE)

    /**
     * The byte order for the stream.
     *
     * Data binding property name: **byte-order**
     */
    public var byteOrder: GDataStreamByteOrder
        get() = g_data_output_stream_get_byte_order(gDataOutputStreamPtr)
        set(value) = g_data_output_stream_set_byte_order(gDataOutputStreamPtr, value)

    override fun close() {
        g_object_unref(gDataOutputStreamPtr)
    }

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GDataOutputStream>?): DataOutputStream = DataOutputStream(ptr)

        public actual fun create(baseStream: OutputStreamBase): DataOutputStream =
            fromPointer(g_data_output_stream_new(baseStream.gOutputStreamPtr))
    }

    public actual fun putByte(data: Byte, cancellable: Cancellable?, error: Error?): Boolean =
        g_data_output_stream_put_byte(
            stream = gDataOutputStreamPtr,
            data = data.toUByte(),
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr)
        ) == TRUE

    public actual fun putInt(data: Int, cancellable: Cancellable?, error: Error?): Boolean =
        g_data_output_stream_put_int32(
            stream = gDataOutputStreamPtr,
            cancellable = cancellable?.gCancellablePtr,
            data = data,
            error = cValuesOf(error?.gErrorPtr)
        ) == TRUE

    public actual fun putUInt(data: UInt, cancellable: Cancellable?, error: Error?): Boolean =
        g_data_output_stream_put_uint32(
            data = data,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr),
            stream = gDataOutputStreamPtr
        ) == TRUE

    public actual fun putLong(data: Long, cancellable: Cancellable?, error: Error?): Boolean =
        g_data_output_stream_put_int64(
            stream = gDataOutputStreamPtr,
            error = cValuesOf(error?.gErrorPtr),
            cancellable = cancellable?.gCancellablePtr,
            data = data
        ) == TRUE

    public actual fun putULong(data: ULong, cancellable: Cancellable?, error: Error?): Boolean =
        g_data_output_stream_put_uint64(
            stream = gDataOutputStreamPtr,
            data = data,
            error = cValuesOf(error?.gErrorPtr),
            cancellable = cancellable?.gCancellablePtr
        ) == TRUE

    public actual fun putShort(data: Short, cancellable: Cancellable?, error: Error?): Boolean =
        g_data_output_stream_put_int16(
            data = data,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr),
            stream = gDataOutputStreamPtr
        ) == TRUE

    public actual fun putUShort(data: UShort, cancellable: Cancellable?, error: Error?): Boolean =
        g_data_output_stream_put_uint16(
            stream = gDataOutputStreamPtr,
            data = data,
            cancellable = cancellable?.gCancellablePtr,
            error = cValuesOf(error?.gErrorPtr)
        ) == TRUE

    public actual fun putString(data: String, cancellable: Cancellable?, error: Error?): Boolean =
        g_data_output_stream_put_string(
            stream = gDataOutputStreamPtr,
            cancellable = cancellable?.gCancellablePtr,
            str = data,
            error = cValuesOf(error?.gErrorPtr)
        ) == TRUE
}
