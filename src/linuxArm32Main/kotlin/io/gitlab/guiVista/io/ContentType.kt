package io.gitlab.guiVista.io

import gio2.*
import glib2.TRUE
import glib2.g_free
import glib2.g_strfreev
import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.io.file.File
import io.gitlab.guiVista.io.icon.Icon
import io.gitlab.guiVista.io.icon.IconBase
import kotlinx.cinterop.*

public actual object ContentType {
    public actual fun equalTypes(type1: String, type2: String): Boolean = g_content_type_equals(type1, type2) == TRUE

    public actual fun isSubset(type: String, superType: String): Boolean = g_content_type_is_a(type, superType) == TRUE

    public actual fun isMimeType(type: String, mimeType: String): Boolean =
        g_content_type_is_mime_type(type, mimeType) == TRUE

    public actual fun isUnknown(type: String): Boolean = g_content_type_is_unknown(type) == TRUE

    public actual fun fetchDescription(type: String): String {
        val ptr = g_content_type_get_description(type)
        val result = ptr?.toKString() ?: ""
        g_free(ptr)
        return result
    }

    public actual fun fetchMimeType(type: String): String {
        val ptr = g_content_type_get_mime_type(type)
        val result = ptr?.toKString() ?: ""
        g_free(ptr)
        return result
    }

    public actual fun fetchIcon(type: String): IconBase = Icon.fromIconPtr(g_content_type_get_icon(type))

    public actual fun fetchSymbolicIcon(type: String): IconBase =
        Icon.fromIconPtr(g_content_type_get_symbolic_icon(type))

    public actual fun fetchGenericIconName(type: String): String {
        val ptr = g_content_type_get_generic_icon_name(type)
        val result = ptr?.toKString() ?: ""
        g_free(ptr)
        return result
    }

    public actual fun canBeExecutable(type: String): Boolean = g_content_type_can_be_executable(type) == TRUE

    public actual fun fromMimeType(mimeType: String): String {
        val ptr = g_content_type_from_mime_type(mimeType)
        val result = ptr?.toKString() ?: ""
        g_free(ptr)
        return result
    }

    public actual fun fetchRegistered(): DoublyLinkedList =
        DoublyLinkedList.fromPointer(g_content_types_get_registered())

    public actual fun guessForTree(root: File): Array<String> {
        val arrayPtr = g_content_type_guess_for_tree(root.gFilePtr)
        val tmpList = mutableListOf<String>()
        var strPtr: CPointer<ByteVar>?
        var pos = 0
        do {
            strPtr = arrayPtr?.get(pos)
            if (strPtr != null) tmpList += strPtr.toKString()
            pos++
        } while (strPtr != null)
        g_strfreev(arrayPtr)
        return tmpList.toTypedArray()
    }

    public actual fun guess(fileName: String, data: UByteArray): Pair<Boolean, String> = memScoped {
        val resultUncertain = alloc<IntVar>()
        val strPtr = g_content_type_guess(
            filename = fileName.ifEmpty { null },
            result_uncertain = resultUncertain.ptr,
            data = data.toCValues(),
            data_size = if (data.isEmpty()) 0u else data.size.toUInt()
        )
        val contentType = strPtr?.toKString() ?: ""
        g_free(strPtr)
        (resultUncertain.value == TRUE) to contentType
    }
}
