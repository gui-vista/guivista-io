package io.gitlab.guiVista.io

import gio2.*
import glib2.TRUE
import glib2.g_object_unref
import glib2.gpointer
import io.gitlab.guiVista.core.*
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.cValuesOf

public actual class Cancellable private constructor(ptr: CPointer<GCancellable>? = null) : ObjectBase {
    public val gCancellablePtr: CPointer<GCancellable>? = ptr ?: g_cancellable_new()

    public actual val isCancelled: Boolean
        get() = g_cancellable_is_cancelled(gCancellablePtr) == TRUE

    public actual val fileDescriptor: Int
        get() = g_cancellable_get_fd(gCancellablePtr)

    public actual companion object {
        public actual val current: Cancellable?
            get() {
                val ptr = g_cancellable_get_current()
                return if (ptr != null) fromPointer(ptr) else null
            }

        public fun fromPointer(ptr: CPointer<GCancellable>?): Cancellable = Cancellable(ptr)

        public actual fun create(): Cancellable = Cancellable()

    }

    public actual fun changeErrorIfCancelled(error: Error): Boolean =
        g_cancellable_set_error_if_cancelled(gCancellablePtr, cValuesOf(error.gErrorPtr)) == TRUE

    public actual fun releaseFileDescriptor() {
        g_cancellable_release_fd(gCancellablePtr)
    }

    public actual fun popCurrent() {
        g_cancellable_pop_current(gCancellablePtr)
    }

    public actual fun pushCurrent() {
        g_cancellable_push_current(gCancellablePtr)
    }

    public actual fun reset() {
        g_cancellable_reset(gCancellablePtr)
    }

    public actual fun cancel() {
        g_cancellable_cancel(gCancellablePtr)
    }

    override fun close() {
        g_object_unref(gCancellablePtr)
    }

    override fun disconnectEvent(handlerId: ULong) {
        disconnectGSignal(gCancellablePtr, handlerId)
    }

    /**
     * Connects the *cancelled* event to a [handler] on a [Cancellable]. This event is used when the operation has been
     * cancelled. Can be used by implementations of cancellable operations. If the operation is cancelled from another
     * thread then the event will be fired in the thread that cancelled the operation, not the thread that is running
     * the operation. Note that disconnecting from this event (or any event) in a multi-threaded program is prone to
     * race conditions. For instance it is possible that a event handler may be invoked even after a call to
     * `g_signal_handler_disconnect()` for that handler has already returned.
     *
     * There is also a problem when cancellation happens right before connecting to the event. If this happens the
     * event will unexpectedly not be emitted, and checking before connecting to the event leaves a race condition
     * where this is still happening. In order to make it safe and easy to connect handlers there are two helper
     * functions: `g_cancellable_connect()` and `g_cancellable_disconnect()` which protect against problems like this.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectCancelledEvent(handler: CPointer<CancelledHandler>,
                                     userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gCancellablePtr, signal = CancellableEvent.cancelled, slot = handler, data = userData)
}

/**
 * The event handler for the *cancelled* event. Arguments:
 * 1. cancellable: CPointer<GCancellable>
 * 2. userData: gpointer
 */
public typealias CancelledHandler = CFunction<(cancellable: CPointer<GCancellable>, userData: gpointer) -> Unit>
