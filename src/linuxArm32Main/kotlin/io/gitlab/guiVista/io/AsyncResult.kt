package io.gitlab.guiVista.io

import gio2.GAsyncResult
import gio2.g_async_result_get_source_object
import gio2.g_async_result_get_user_data
import gio2.g_async_result_is_tagged
import glib2.TRUE
import glib2.gpointer
import io.gitlab.guiVista.core.Object
import io.gitlab.guiVista.core.ObjectBase
import kotlinx.cinterop.CPointer

public actual interface AsyncResult {
    public val gAsyncResultPtr: CPointer<GAsyncResult>?
    public actual val srcObj: ObjectBase?
        get() {
            val ptr = g_async_result_get_source_object(gAsyncResultPtr)
            return if (ptr != null) Object.fromPointer(ptr) else null
        }

    /** The user data from a [AsyncResult]. */
    public val userData: gpointer?
        get() = g_async_result_get_user_data(gAsyncResultPtr)

    /**
     * Checks if this [AsyncResult] has the given [srcTag] (generally a function pointer indicating the
     * [function][AsyncResult] was created by).
     * @param srcTag An application defined tag.
     * @return A value of *true* if this [AsyncResult] has the indicated [srcTag].
     */
    public fun isTagged(srcTag: gpointer?): Boolean = g_async_result_is_tagged(gAsyncResultPtr, srcTag) == TRUE
}
