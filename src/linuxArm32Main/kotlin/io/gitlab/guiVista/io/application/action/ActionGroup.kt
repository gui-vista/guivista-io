package io.gitlab.guiVista.io.application.action

import gio2.GActionGroup
import glib2.g_object_unref
import kotlinx.cinterop.CPointer

public actual class ActionGroup private constructor(ptr: CPointer<GActionGroup>?) : ActionGroupBase {
    override val gActionGroupPtr: CPointer<GActionGroup>? = ptr

    public companion object {
        public fun fromPointer(ptr: CPointer<GActionGroup>?): ActionGroup = ActionGroup(ptr)
    }

    override fun close() {
        g_object_unref(gActionGroupPtr)
    }
}
