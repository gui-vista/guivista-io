package io.gitlab.guiVista.io.application

import gio2.*
import glib2.TRUE
import glib2.g_object_unref
import glib2.gpointer
import kotlinx.cinterop.*
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.io.application.action.ActionGroupBase
import io.gitlab.guiVista.io.application.action.ActionMap
import io.gitlab.guiVista.io.file.File

public actual interface ApplicationBase : ActionMap, ActionGroupBase, ObjectBase {
    public val gApplicationPtr: CPointer<GApplication>?
    override val gActionMapPtr: CPointer<GActionMap>?
        get() = gApplicationPtr?.reinterpret()
    override val gActionGroupPtr: CPointer<GActionGroup>?
        get() = gApplicationPtr?.reinterpret()

    public actual var flags: UInt
        get() = g_application_get_flags(gApplicationPtr)
        set(value) = g_application_set_flags(gApplicationPtr, value)

    public actual val appId: String
        get() = g_application_get_application_id(gApplicationPtr)?.toKString() ?: ""

    public actual var inactivityTimeout: UInt
        get() = g_application_get_inactivity_timeout(gApplicationPtr)
        set(value) = g_application_set_inactivity_timeout(gApplicationPtr, value)

    public actual var resourceBasePath: String
        get() = g_application_get_resource_base_path(gApplicationPtr)?.toKString() ?: ""
        set(value) = g_application_set_resource_base_path(gApplicationPtr, value)

    public actual val isBusy: Boolean
        get() = g_application_get_is_busy(gApplicationPtr) == TRUE

    public actual val isRegistered: Boolean
        get() = g_application_get_is_registered(gApplicationPtr) == TRUE

    public actual val isRemote: Boolean
        get() = g_application_get_is_remote(gApplicationPtr) == TRUE

    public actual fun hold() {
        g_application_hold(gApplicationPtr)
    }

    public actual fun release() {
        g_application_release(gApplicationPtr)
    }

    public actual fun quit() {
        g_application_quit(gApplicationPtr)
    }

    public actual fun open(files: Array<File>, hint: String) {
        if (files.isEmpty()) throw IllegalArgumentException("The files parameter cannot be empty.")
        val tmp = files.map { it.gFilePtr }.toTypedArray()
        g_application_open(application = gApplicationPtr, files = cValuesOf(*tmp), n_files = files.size, hint = hint)
    }

    public actual fun markBusy() {
        g_application_mark_busy(gApplicationPtr)
    }

    public actual fun unmarkBusy() {
        g_application_unmark_busy(gApplicationPtr)
    }

    /**
     * Connects the *activate* event to a [handler] on a application. This event is used for initialising the
     * application window, and is emitted on the primary instance when an activation occurs.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     * @return The handler ID.
     */
    public fun connectActivateEvent(handler: CPointer<ActivateHandler>,
                                    userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gApplicationPtr, signal = ApplicationBaseEvent.activate, slot = handler, data = userData)

    /**
     * Connects the *startup* event to a [handler] on a application. This event is emitted on the primary
     * instance immediately after registration.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     * @return The handler ID.
     */
    public fun connectStartupEvent(handler: CPointer<StartupHandler>,
                                   userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gApplicationPtr, signal = ApplicationBaseEvent.startup, slot = handler, data = userData)

    /**
     * Connects the *shutdown* event to a [handler] on a application. The *shutdown* event is emitted only on the
     * registered primary instance immediately after the main loop terminates.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     * @return The handler ID.
     */
    public fun connectShutdownEvent(handler: CPointer<ShutdownHandler>,
                                    userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gApplicationPtr, signal = ApplicationBaseEvent.shutdown, slot = handler, data = userData)

    /**
     * Connects the *open* event to a [handler] on a application. The *open* event is emitted only when there are files
     * to open. Refer to [open][ApplicationBase.open] for more information.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     * @return The handler ID.
     */
    public fun connectOpenEvent(handler: CPointer<OpenHandler>,
                                userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gApplicationPtr, signal = ApplicationBaseEvent.open, slot = handler, data = userData)

    public actual fun run(): Int = g_application_run(gApplicationPtr, 0, null)

    override fun disconnectEvent(handlerId: ULong) {
        super<ActionGroupBase>.disconnectEvent(handlerId)
        disconnectGSignal(gApplicationPtr, handlerId)
    }

    public actual fun sendNotification(id: String, notification: Notification) {
        g_application_send_notification(application = gApplicationPtr, id = id,
            notification = notification.gNotificationPtr)
    }

    public actual fun withdrawNotification(id: String) {
        g_application_withdraw_notification(gApplicationPtr, id)
    }

    override fun close() {
        g_object_unref(gApplicationPtr)
    }

    public actual companion object {
        public actual fun validAppId(id: String): Boolean = g_application_id_is_valid(id) == TRUE
    }
}

/**
 * The event handler for the *activate* event. Arguments:
 * 1. application: CPointer<GApplication>
 * 2. userData: gpointer
 */
public typealias ActivateHandler = CFunction<(application: CPointer<GApplication>, userData: gpointer) -> Unit>

/**
 * The event handler for the *startup* event. Arguments:
 * 1. application: CPointer<GApplication>
 * 2. userData: gpointer
 */
public typealias StartupHandler = CFunction<(application: CPointer<GApplication>, userData: gpointer) -> Unit>

/**
 * The event handler for the *shutdown* event. Arguments:
 * 1. application: CPointer<GApplication>
 * 2. userData: gpointer
 */
public typealias ShutdownHandler = CFunction<(application: CPointer<GApplication>, userData: gpointer) -> Unit>

/**
 * The event handler for the *open* event that occurs when there are files to open. Arguments:
 * 1. application: CPointer<GApplication>
 * 2. files: gpointer
 * 3. totalFiles: Int
 * 4. hint: CPointer<ByteVar>
 * 5. userData: gpointer
 */
public typealias OpenHandler = CFunction<(
    application: CPointer<GApplication>,
    files: gpointer,
    totalFiles: Int,
    hint: CPointer<ByteVar>,
    userData: gpointer
) -> Unit>
