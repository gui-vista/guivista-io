package io.gitlab.guiVista.io.application.action

import gio2.*
import glib2.TRUE
import kotlinx.cinterop.*
import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.dataType.variant.Variant
import io.gitlab.guiVista.core.dataType.variant.VariantType

public actual interface Action {
    public val gActionPtr: CPointer<GAction>?

    public actual val name: String
        get() = g_action_get_name(gActionPtr)?.toKString() ?: ""

    public actual val paramType: VariantType?
        get() {
            val ptr = g_action_get_parameter_type(gActionPtr)
            return if (ptr != null) VariantType.fromPointer(ptr) else null
        }

    public actual val stateType: VariantType?
        get() {
            val ptr = g_action_get_state_type(gActionPtr)
            return if (ptr != null) VariantType.fromPointer(ptr) else null
        }

    public actual val stateHint: Variant?
        get() {
            val ptr = g_action_get_state_hint(gActionPtr)
            return if (ptr != null) Variant.fromPointer(ptr) else null
        }

    public actual val enabled: Boolean
        get() = g_action_get_enabled(gActionPtr) == TRUE

    public actual fun changeState(value: Variant) {
        g_action_change_state(gActionPtr, value.gVariantPtr)
    }

    public actual fun fetchState(): Variant? {
        val ptr = g_action_get_state(gActionPtr)
        return if (ptr != null) Variant.fromPointer(ptr) else null
    }

    public actual fun activate(actionParam: Variant?) {
        g_action_activate(gActionPtr, actionParam?.gVariantPtr)
    }

    public actual fun parseDetailedName(
        detailedName: String,
        actionName: String,
        targetValue: Variant?,
        error: Error?
    ): Boolean = memScoped {
        // TODO: Cover error handling.
        return g_action_parse_detailed_name(
            detailed_name = detailedName,
            action_name = cValuesOf(actionName.cstr.ptr),
            target_value = cValuesOf(targetValue?.gVariantPtr),
            error = null
        ) == TRUE
    }

    public actual fun printDetailedName(actionName: String, targetValue: Variant?): String =
        g_action_print_detailed_name(actionName, targetValue?.gVariantPtr)?.toKString() ?: ""
}
