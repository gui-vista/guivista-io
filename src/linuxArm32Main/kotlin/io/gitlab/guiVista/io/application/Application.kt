package io.gitlab.guiVista.io.application

import gio2.GApplication
import gio2.G_APPLICATION_FLAGS_NONE
import gio2.g_application_id_is_valid
import gio2.g_application_new
import glib2.FALSE
import kotlinx.cinterop.CPointer

public actual class Application private constructor(
    ptr: CPointer<GApplication>? = null,
    id: String,
    flags: UInt = G_APPLICATION_FLAGS_NONE
) : ApplicationBase {
    override val gApplicationPtr: CPointer<GApplication>? = ptr ?: g_application_new(id, flags)

    public actual companion object {
        public actual fun create(id: String, flags: UInt): Application = if (g_application_id_is_valid(id) == FALSE) {
            throw IllegalArgumentException("Application ID isn't valid.")
        } else {
            Application(id = id, flags = flags)
        }

        public fun fromPointer(ptr: CPointer<GApplication>?): Application = Application(ptr = ptr, id = "")
    }
}
