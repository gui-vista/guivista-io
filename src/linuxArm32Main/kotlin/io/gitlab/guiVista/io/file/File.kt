package io.gitlab.guiVista.io.file

import gio2.*
import glib2.TRUE
import glib2.g_object_unref
import kotlinx.cinterop.CPointer

@Suppress("EqualsOrHashCode")
public actual class File private constructor(ptr: CPointer<GFile>?) : FileBase {
    public override val gFilePtr: CPointer<GFile>? = ptr

    public actual companion object {
        /** Creates a new [File] instance from a filePtr. */
        public fun fromPointer(filePtr: CPointer<GFile>?): File = File(filePtr)

        public actual fun fromPath(path: String): File = File(g_file_new_for_path(path))

        public actual fun fromUri(uri: String): File = File(g_file_new_for_uri(uri))

        public actual fun parseName(parseName: String): File = File(g_file_parse_name(parseName))

        public actual fun fromCommandLine(arg: String, cwd: String): File =
            if (cwd.isEmpty()) File(g_file_new_for_commandline_arg(arg))
            else File(g_file_new_for_commandline_arg_and_cwd(arg, cwd))
    }

    actual override fun hashCode(): Int = g_file_hash(gFilePtr).toInt()

    actual override fun equals(other: Any?): Boolean =
        if (other is File) g_file_equal(gFilePtr, other.gFilePtr) == TRUE else false

    override fun close() {
        g_object_unref(gFilePtr)
    }
}
