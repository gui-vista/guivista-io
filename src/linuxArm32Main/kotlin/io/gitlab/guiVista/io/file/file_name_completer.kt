package io.gitlab.guiVista.io.file

import gio2.*
import glib2.*
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import kotlinx.cinterop.*

public actual class FileNameCompleter private constructor(ptr: CPointer<GFilenameCompleter>? = null) : ObjectBase {
    public val gFilenameCompleterPtr: CPointer<GFilenameCompleter>? = ptr ?: g_filename_completer_new()

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GFilenameCompleter>?): FileNameCompleter = FileNameCompleter(ptr)

        public actual fun create(): FileNameCompleter = FileNameCompleter()
    }

    public actual fun fetchCompletionSuffix(initialText: String): String {
        val ptr = g_filename_completer_get_completion_suffix(gFilenameCompleterPtr, initialText)
        val result = ptr?.toKString() ?: ""
        g_free(ptr)
        return result
    }

    public actual fun fetchCompletions(initialText: String): Array<String> {
        val arrayPtr = g_filename_completer_get_completions(gFilenameCompleterPtr, initialText)
        val tmpList = mutableListOf<String>()
        var strPtr: CPointer<ByteVar>?
        var pos = 0
        do {
            strPtr = arrayPtr?.get(pos)
            if (strPtr != null) tmpList += strPtr.toKString()
            pos++
        } while (strPtr != null)
        g_strfreev(arrayPtr)
        return tmpList.toTypedArray()
    }

    public actual fun changeDirsOnly(dirsOnly: Boolean) {
        g_filename_completer_set_dirs_only(gFilenameCompleterPtr, if (dirsOnly) TRUE else FALSE)
    }

    override fun close() {
        g_object_unref(gFilenameCompleterPtr)
    }

    /**
     * Connects the *got-completion-data* event to a [handler] on a [FileNameCompleter]. This event is used when the
     * file name completion information is available.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectGotCompletionDataEvent(
        handler: CPointer<GotCompletionDataHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gFilenameCompleterPtr, signal = FileNameCompleterEvent.gotCompletionData, slot = handler,
            data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gFilenameCompleterPtr, handlerId)
    }
}

/**
 * The event handler for the *got-completion-data* event. Arguments:
 * 1. fileNameCompleter: CPointer<GFilenameCompleter>
 * 2. userData: gpointer
 */
public typealias GotCompletionDataHandler = CFunction<(
    fileNameCompleter: CPointer<GFilenameCompleter>,
    userData: gpointer
) -> Unit>
