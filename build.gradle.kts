import java.util.Properties
import org.jetbrains.dokka.gradle.DokkaTask

val gitLabSettings = fetchGitLabSettings()
val projectSettings = fetchProjectSettings()

group = "io.gitlab.gui-vista"
version = if (projectSettings.isDevVer) "${projectSettings.libVer}-dev" else projectSettings.libVer

plugins {
    kotlin("multiplatform") version "1.4.31"
    id("org.jetbrains.dokka") version "1.4.32"
    `maven-publish`
    signing
}

ext["signing.keyId"] = null
ext["signing.password"] = null
ext["signing.secretKeyRingFile"] = null
ext["ossrhUsername"] = null
ext["ossrhPassword"] = null

val secretPropsFile = project.rootProject.file("maven_central.properties")
if (secretPropsFile.exists()) {
    secretPropsFile.reader().use { reader ->
        Properties().apply {
            load(reader)
        }
    }.onEach { (name, value) ->
        ext["$name"] = value
    }
} else {
    ext["signing.keyId"] = System.getenv("SIGNING_KEY_ID") ?: ""
    ext["signing.password"] = System.getenv("SIGNING_PASSWORD") ?: ""
    ext["signing.secretKeyRingFile"] = System.getenv("SIGNING_SECRET_KEY_RING_FILE") ?: ""
    ext["ossrhUsername"] = System.getenv("OSSRH_USERNAME") ?: ""
    ext["ossrhPassword"] = System.getenv("OSSRH_PASSWORD") ?: ""
}

repositories {
    mavenCentral()
    mavenLocal()
}

val dokkaHtml by tasks.getting(DokkaTask::class)

val javadocJar by tasks.register("javadocJar", Jar::class) {
    dependsOn(dokkaHtml)
    archiveClassifier.set("javadoc")
    from(dokkaHtml.outputDirectory)
}

kotlin {
    val guiVistaVer = if (!projectSettings.isDevVer) projectSettings.libVer else "${projectSettings.libVer}-dev"
    explicitApi()
    linuxX64("linuxX64") {
        compilations.getByName("main") {
            dependencies {
                implementation("$group:guivista-core:$guiVistaVer")
            }
            cinterops.create("gio2") {
                includeDirs("/usr/include/glib-2.0/gio")
            }
        }
    }

    linuxArm32Hfp("linuxArm32") {
        compilations.getByName("main") {
            dependencies {
                implementation("$group:guivista-core:$guiVistaVer")
            }
            cinterops.create("gio2") {
                includeDirs("/mnt/pi_image/usr/include/glib-2.0/gio")
            }
        }
    }

    sourceSets {
        val unsignedTypes = "kotlin.ExperimentalUnsignedTypes"

        @Suppress("UNUSED_VARIABLE")
        val commonMain by getting {
            languageSettings.useExperimentalAnnotation(unsignedTypes)
            dependencies {
                val kotlinVer = "1.4.31"
                implementation(kotlin("stdlib-common", kotlinVer))
                implementation("$group:guivista-core:$guiVistaVer")
            }
        }

        @Suppress("UNUSED_VARIABLE")
        val linuxX64Main by getting {
            languageSettings.useExperimentalAnnotation(unsignedTypes)
        }

        @Suppress("UNUSED_VARIABLE")
        val linuxArm32Main by getting {
            languageSettings.useExperimentalAnnotation(unsignedTypes)
        }
    }
}

publishing {
    val publishToMavenCentral = getExtraString("mavenCentral.publishingEnabled")?.toBoolean() ?: false
    publications.withType<MavenPublication> {
        if (projectSettings.includeDocs) artifact(javadocJar)
        createPom()
    }
    repositories {
        if (gitLabSettings.publishingEnabled) {
            createGitLabRepo(projectId = gitLabSettings.projectId, token = gitLabSettings.token)
        }
        if (publishToMavenCentral) createMavenCentralRepo()
    }
}

tasks.create("createLinuxLibraries") {
    dependsOn("linuxX64MainKlibrary", "linuxArm32MainKlibrary")
}

tasks.getByName("publish") {
    doFirst { println("Project Version: ${project.version}") }
}

data class GitLabSettings(val token: String, val projectId: Int, val publishingEnabled: Boolean)

fun fetchGitLabSettings(): GitLabSettings {
    var token = ""
    var projectId = -1
    val properties = Properties()
    var publishingEnabled = true
    file("gitlab.properties").inputStream().use { inputStream ->
        properties.load(inputStream)
        publishingEnabled = properties.getProperty("publishingEnabled")?.toBoolean() ?: true
        token = properties.getProperty("token") ?: ""
        @Suppress("RemoveSingleExpressionStringTemplate")
        projectId = "${properties.getProperty("projectId")}".toInt()
    }
    return GitLabSettings(token = token, projectId = projectId, publishingEnabled = publishingEnabled)
}

data class ProjectSettings(val libVer: String, val isDevVer: Boolean, val includeDocs: Boolean)

fun fetchProjectSettings(): ProjectSettings {
    var libVer = "SNAPSHOT"
    var isDevVer = true
    var includeDocs = false
    val properties = Properties()
    file("project.properties").inputStream().use { inputStream ->
        properties.load(inputStream)
        libVer = properties.getProperty("libVer") ?: "SNAPSHOT"
        @Suppress("RemoveSingleExpressionStringTemplate")
        isDevVer = "${properties.getProperty("isDevVer")}".toBoolean()
        @Suppress("RemoveSingleExpressionStringTemplate")
        includeDocs = "${properties.getProperty("includeDocs")}".toBoolean()
    }
    return ProjectSettings(libVer = libVer, isDevVer = isDevVer, includeDocs = includeDocs)
}

val Boolean.intValue: Int
    get() = if (this) 1 else 0

fun PublishingExtension.createGitLabRepository() {
    repositories.maven {
        val projectId = gitLabSettings.projectId
        url = uri("https://gitlab.com/api/v4/projects/$projectId/packages/maven")
        credentials(HttpHeaderCredentials::class.java) {
            name = "Private-Token"
            value = gitLabSettings.token
        }
        authentication {
            create("header", HttpHeaderAuthentication::class.java)
        }
    }
}

fun getExtraString(name: String) = ext[name]?.toString()

fun MavenPublication.createPom() = pom {
    name.set("GUI Vista IO")
    description.set("A Kotlin Native library that provides Input/Output functionality in a Kotlin Native project.")
    url.set("https://gitlab.com/gui-vista/guivista-io")

    licenses {
        license {
            name.set("Apache 2.0")
            url.set("https://opensource.org/licenses/Apache-2.0")
        }
    }
    developers {
        developer {
            id.set("NickApperley")
            name.set("Nick Apperley")
            email.set("napperley@protonmail.com")
        }
    }
    scm {
        url.set("https://gitlab.com/gui-vista/guivista-io")
    }
}

fun RepositoryHandler.createMavenCentralRepo() {
    maven {
        name = "sonatype"
        setUrl("https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/")
        credentials {
            username = getExtraString("ossrhUsername")
            password = getExtraString("ossrhPassword")
        }
    }
}

fun RepositoryHandler.createGitLabRepo(projectId: Int, token: String) {
    maven {
        name = "Git Lab"
        url = uri("https://gitlab.com/api/v4/projects/$projectId/packages/maven")
        credentials(HttpHeaderCredentials::class.java) {
            name = "Private-Token"
            value = token
        }
        authentication {
            create("header", HttpHeaderAuthentication::class.java)
        }
    }
}

signing {
    sign(publishing.publications)
}
